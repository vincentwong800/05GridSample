// 05dll.cpp : 定义 DLL 应用程序的入口点。
//

#include "stdafx.h"
#include "05dll.h"


#ifdef _MANAGED
#pragma managed(push, off)
#endif

BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
					 )
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
		{
			TCHAR szModule[MAX_PATH] = {0};
			GetModuleFileName(hModule, szModule, MAX_PATH-1);
			Output::DbgPrint(TEXT("DLL_PROCESS_ATTACH,[%s]"), szModule);
		} break;

	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
	case DLL_PROCESS_DETACH:
		break;
	}
    return TRUE;
}

#ifdef _MANAGED
#pragma managed(pop)
#endif

// 这是导出变量的一个示例
MY05DLL_API int nMy05dll=0;

// 这是导出函数的一个示例。
MY05DLL_API int fnMy05dll(void)
{
	return 42;
}

// 这是已导出类的构造函数。
// 有关类定义的信息，请参阅 05dll.h
CMy05dll::CMy05dll()
{
	return;
}
