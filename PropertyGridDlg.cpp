// PropertyGridDlg.cpp : implementation file
//
// This file is a part of the XTREME TOOLKIT PRO MFC class library.
// (c)1998-2011 Codejock Software, All Rights Reserved.
//
// THIS SOURCE FILE IS THE PROPERTY OF CODEJOCK SOFTWARE AND IS NOT TO BE
// RE-DISTRIBUTED BY ANY MEANS WHATSOEVER WITHOUT THE EXPRESSED WRITTEN
// CONSENT OF CODEJOCK SOFTWARE.
//
// THIS SOURCE CODE CAN ONLY BE USED UNDER THE TERMS AND CONDITIONS OUTLINED
// IN THE XTREME TOOLKIT PRO LICENSE AGREEMENT. CODEJOCK SOFTWARE GRANTS TO
// YOU (ONE SOFTWARE DEVELOPER) THE LIMITED RIGHT TO USE THIS SOFTWARE ON A
// SINGLE COMPUTER.
//
// CONTACT INFORMATION:
// support@codejock.com
// http://www.codejock.com
//
/////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "PropertyGrid.h"
#include "PropertyGridDlg.h"

#include "bin/vc80/EncryString.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPropertyGridDlg dialog

CPropertyGridDlg::CPropertyGridDlg(CWnd* pParent /*=NULL*/)
: CPropertyGridDlgBase(CPropertyGridDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);

	m_bCollapse = FALSE;
	m_setfile = TString(TEXT("settings.xml"));
}

void CPropertyGridDlg::DoDataExchange(CDataExchange* pDX)
{
	CPropertyGridDlgBase::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPropertyGridDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CPropertyGridDlg, CPropertyGridDlgBase)
	//{{AFX_MSG_MAP(CPropertyGridDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_MESSAGE(XTPWM_PROPERTYGRID_NOTIFY, OnGridNotify)

	ON_WM_TIMER()
//	ON_BN_CLICKED(IDC_BUTTON1, &CPropertyGridDlg::OnBnClickedButton1)
//	ON_BN_CLICKED(IDC_BUTTON2, &CPropertyGridDlg::OnBnClickedButton2)
//	ON_BN_CLICKED(IDC_BUTTON3, &CPropertyGridDlg::OnBnClickedButton3)
//	ON_BN_CLICKED(IDC_BUTTON4, &CPropertyGridDlg::OnBnClickedButton4)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPropertyGridDlg message handlers

//////////////////////////////////////////////////////////////////////////
// hooks
Undocument::fnNtQueryInformationProcess _NtQueryInformationProcess = Undocument::NtQueryInformationProcess;
NTSTATUS WINAPI _NtQueryInformationProcess_Hook(
	_In_      HANDLE           ProcessHandle,
	_In_      Common::PROCESS_INFORMATION_CLASS ProcessInformationClass,
	_Out_     PVOID            ProcessInformation,
	_In_      ULONG            ProcessInformationLength,
	_Out_opt_ PULONG           ReturnLength
	)
{
	PVOID caller = _caller();
	NTSTATUS n;
	__asm {
		push ReturnLength
			push ProcessInformationLength
			push ProcessInformation
			push ProcessInformationClass
			push ProcessHandle
			call _NtQueryInformationProcess
			mov n, eax
	}
	DbgPrintA("%s, %p, %p,%p,%p,%p", FunctionA, caller, ProcessHandle, ProcessInformationClass, ProcessInformation, ProcessInformationLength);
	return n;
}

Undocument::fnNtReadVirtualMemory _NtReadVirtualMemory = Undocument::NtReadVirtualMemory;
NTSTATUS NTAPI _NtReadVirtualMemory_Hook(
	IN HANDLE               ProcessHandle,
	IN PVOID                BaseAddress,
	OUT PVOID               Buffer,
	IN ULONG                NumberOfBytesToRead,
	OUT PULONG              NumberOfBytesReaded OPTIONAL
	)
{
	PVOID caller = _caller();
	NTSTATUS n;
	__asm {
		push NumberOfBytesReaded
			push NumberOfBytesToRead
			push Buffer
			push BaseAddress
			push ProcessHandle
			call _NtReadVirtualMemory
			mov n, eax
	}
	DbgPrintA("%s, %p, %p,%p,%p,%p", FunctionA, caller, ProcessHandle, BaseAddress, Buffer, NumberOfBytesToRead);
	return n;
}

Undocument::fnNtQueryVirtualMemory _NtQueryVirtualMemory = Undocument::NtQueryVirtualMemory;
NTSTATUS NTAPI _NtQueryVirtualMemory_Hook(
	IN HANDLE               ProcessHandle,
	IN PVOID                BaseAddress,
	IN Common::MEMORY_INFORMATION_CLASS MemoryInformationClass,
	OUT PVOID               Buffer,
	IN ULONG                aLength,
	OUT PULONG              ResultLength OPTIONAL
	)
{
	PVOID caller = _caller();
	NTSTATUS n;
	__asm {
		push ResultLength
			push aLength
			push Buffer
			push MemoryInformationClass
			push BaseAddress
			push ProcessHandle
			call _NtQueryVirtualMemory
			mov n, eax
	}
	DbgPrintA("%s, %p, %p,%p,%p,%p,%p", FunctionA, caller, ProcessHandle, BaseAddress, MemoryInformationClass, Buffer, aLength);
	return n;
}

Undocument::fnNtCreateSection _NtCreateSection = Undocument::NtCreateSection;
NTSTATUS NTAPI _NtCreateSection_Hook(
									 OUT PHANDLE             SectionHandle,
									 IN ULONG                DesiredAccess,
									 IN POBJECT_ATTRIBUTES   ObjectAttributes OPTIONAL,
									 IN PLARGE_INTEGER       MaximumSize OPTIONAL,
									 IN ULONG                PageAttributess,
									 IN ULONG                SectionAttributes,
									 IN HANDLE               FileHandle OPTIONAL )
{
	PVOID caller = _caller();
	NTSTATUS n;
	__asm {
		push FileHandle
			push SectionAttributes
			push PageAttributess
			push MaximumSize
			push ObjectAttributes
			push DesiredAccess
			push SectionHandle
			call _NtCreateSection
			mov n, eax
	}
	if (ObjectAttributes)
	{
		DbgPrintW(L"object:%wZ", ObjectAttributes->ObjectName);
	}
	DbgPrintA("%s, n:%p, %p, %p,%p,%p,%p,%p,%p,%p", FunctionA, n, caller, SectionHandle,DesiredAccess,ObjectAttributes,MaximumSize,PageAttributess,SectionAttributes,FileHandle);
	return n;
}

Undocument::fnNtMapViewOfSection _NtMapViewOfSection = Undocument::NtMapViewOfSection;
NTSTATUS NTAPI _NtMapViewOfSection_Hook(
										IN HANDLE               SectionHandle,
										IN HANDLE               ProcessHandle,
										IN OUT PVOID            *BaseAddress OPTIONAL,
										IN ULONG                ZeroBits OPTIONAL,
										IN ULONG                CommitSize,
										IN OUT PLARGE_INTEGER   SectionOffset OPTIONAL,
										IN OUT PULONG           ViewSize,
										IN Common::SECTION_INHERIT      InheritDisposition,
										IN ULONG                AllocationType OPTIONAL,
										IN ULONG                Protect )
{
	PVOID caller = _caller();
	NTSTATUS n;
	__asm {
		push Protect
			push AllocationType
			push InheritDisposition
			push ViewSize
			push SectionOffset
			push CommitSize
			push ZeroBits
			push BaseAddress
			push ProcessHandle
			push SectionHandle
			call _NtMapViewOfSection
			mov n, eax
	}
	if (!(Protect == 4 /*|| Protect == 6 || Protect == 0x10*/))
	{
		DbgPrintA("%s, %p", FunctionA, caller);
		if (BaseAddress)
			DbgPrintA("	BaseAddress:%p", *BaseAddress);
		else
			DbgPrintA("	BaseAddress:%p", BaseAddress);
		DbgPrintA("	CommitSize:%p", CommitSize);
		if (SectionOffset)
			DbgPrintA("	SectionOffset:%I64p", SectionOffset->QuadPart);
		else
			DbgPrintA("	SectionOffset:%p", SectionOffset);
		if (ViewSize)
			DbgPrintA("	ViewSize:%p", *ViewSize);
		else
			DbgPrintA("	ViewSize:%p", ViewSize);
	}
	return n;
}

Undocument::fnNtQueryInformationThread _NtQueryInformationThread = Undocument::NtQueryInformationThread;
NTSTATUS NTAPI  _NtQueryInformationThread_Hook(
	IN HANDLE               ThreadHandle,
	IN Common::THREAD_INFORMATION_CLASS ThreadInformationClass,
	OUT PVOID               ThreadInformation,
	IN ULONG                ThreadInformationLength,
	OUT PULONG              ReturnLength OPTIONAL
	)
{
	PVOID caller = _caller();
	NTSTATUS n;
	__asm {
			push ReturnLength
			push ThreadInformationLength
			push ThreadInformation
			push ThreadInformationClass
			push ThreadHandle
			call _NtQueryInformationThread
			mov n, eax
	}
	DbgPrintA("%s, %p, %p,%p,%p,%p", FunctionA, caller, ThreadHandle,ThreadInformationClass,ThreadInformation,ThreadInformationLength);
	return n;
}

//////////////////////////////////////////////////////////////////////////

LONG WINAPI FilterFunction(struct _EXCEPTION_POINTERS* ExceptionInfo, TCHAR* file, int line)
{
	DbgPrint(TEXT("exp:%s(%d), Addr:%p, eCode:%p"),
		file, line,
		ExceptionInfo->ExceptionRecord->ExceptionAddress,
		ExceptionInfo->ExceptionRecord->ExceptionCode);
	return EXCEPTION_EXECUTE_HANDLER;
}

void except2() {
	CStdioFile sf;
	sf.Open(TEXT("kkk.txt"), CFile::modeRead|CFile::typeText);
	UCHAR buf[100];
	sf.Read(buf, 100);
	sf.Close();
}

void t2()
{
	int * p = NULL;
	*p = 1;
}
void t1()
{
	t2();
}
void except1() {
	__try
	{
		t1();
		//except2();
	} exceptStackTracer();

	//try
	//{
	//	t1();
	//	//except2();
	//} catchSeException();
	
	//try {
	//	//int m = 1-1;
	//	//int n = 5/m;
	//	//DbgPrint(TEXT("m:%d, m:%d"), m, n);

	//	char *p = NULL;
	//	*p = 1;

	//	EXCEPTION_ACCESS_VIOLATION;

	//	//except2();
	//}
	//catch_all();
	//catchSeException();
}

LONG WINAPI MyUnhandledExceptionFilter(struct _EXCEPTION_POINTERS* ExceptionInfo)
{
	DbgPrint(TEXT("unhandlerexp..Addr:%p, eCode:%p"),
		ExceptionInfo->ExceptionRecord->ExceptionAddress,
		ExceptionInfo->ExceptionRecord->ExceptionCode);
	return EXCEPTION_EXECUTE_HANDLER;
}

LONG CALLBACK VectoredHandler(PEXCEPTION_POINTERS ExceptionInfo)
{
	DbgPrint(TEXT("vechdl..Addr:%p, eCode:%p"),
		ExceptionInfo->ExceptionRecord->ExceptionAddress,
		ExceptionInfo->ExceptionRecord->ExceptionCode);
	return EXCEPTION_EXECUTE_HANDLER;
}

//////////////////////////////////////////////////////////////////////////
typedef struct _rvm_data {
	int index;
	DWORD64 pid;
	DWORD64 BaseAddress;
	DWORD64 Buffer;
	DWORD64 NumberOfBytesToRead;
	DWORD64 caller;
} rvm_data;
vector<rvm_data> g_vec_rvm;
int g_rvm = 0;

UINT CALLBACK Thread_Out(LPVOID p)
{
	do 
	{
		::Sleep(100);
		for (size_t i=0; i<g_vec_rvm.size(); i++)
		{
			vector<rvm_data>::iterator p = g_vec_rvm.begin();
			Output::DbgPrint(TEXT("%d bcrvm, %I64p, %I64d,%I64p,%I64p,%I64p"),
				p->index,
				p->caller,
				p->pid,
				p->BaseAddress,
				p->Buffer,
				p->NumberOfBytesToRead);
			g_vec_rvm.erase(p);
			break;
		}
	} while (TRUE);
	return 0;
}

// hook NtReadVirtualMemory64
reg64 Old_NtReadVirtualMemory64;
NTSTATUS __stdcall rptNtReadVirtualMemory64_Hook(PX64CallStack pX64Stack)
{
	NTSTATUS bOk = Wow64Hook_STATUS_Continue;
	static DWORD64 GetProcessId64 = GetProcAddress64(g_hKernel3264, "GetProcessId");

	DWORD64 pid = X64Call(GetProcessId64, 1, pX64Stack->rcx.v);

	//rvm_data rd;
	//rd.index = g_rvm++;
	//rd.pid = pid;
	//rd.BaseAddress = pX64Stack->rdx.v;
	//rd.Buffer = pX64Stack->r8.v;
	//rd.NumberOfBytesToRead = pX64Stack->r9.v;
	//rd.caller = pX64Stack->para[0].v;
	//g_vec_rvm.push_back(rd);

	return bOk;
}
VOID NAKED New_NtReadVirtualMemory64()
{
	__asm {
		X64_Pushad();
		X64_End();

		pushfd
			pushad
			lea ebx, dword ptr [esp+0x24]
		push ebx
			call rptNtReadVirtualMemory64_Hook
			cmp eax, Wow64Hook_STATUS_Continue
			je _continue

			popad
			popfd
			X64_Start();
		X64_Popad();
		ret

_continue:
		popad
			popfd
			X64_Start();
		X64_Popad();

		X64_End();
		push Old_NtReadVirtualMemory64.dw[4]
		push Old_NtReadVirtualMemory64.dw[0]
		X64_Start();
		ret
	}
}

UCHAR* prdata = NULL;
ULONG nrdata = 0;

ULONG _NtDeviceIoControlFile = (ULONG)NtDeviceIoControlFile;
NTSTATUS NTAPI _NtDeviceIoControlFile_Hook(
	IN HANDLE               FileHandle,
	IN HANDLE               Event OPTIONAL,
	IN PIO_APC_ROUTINE      ApcRoutine OPTIONAL,
	IN PVOID                ApcContext OPTIONAL,
	OUT PIO_STATUS_BLOCK    IoStatusBlock,
	IN ULONG                IoControlCode,
	IN PVOID                InputBuffer OPTIONAL,
	IN ULONG                InputBufferLength,
	OUT PVOID               OutputBuffer OPTIONAL,
	IN ULONG                OutputBufferLength )
{
	NTSTATUS status;
	__asm {
		push OutputBufferLength
			push OutputBuffer
			push InputBufferLength
			push InputBuffer
			push IoControlCode
			push IoStatusBlock
			push ApcContext
			push ApcRoutine
			push Event
			push FileHandle
			call _NtDeviceIoControlFile
			mov status, eax
	}

	//IOCTL_NDIS_QUERY_GLOBAL_STATS;
	if (IOCTL_GET_DRIVE_INFO == IoControlCode)
	{
		if (OutputBufferLength)
		{
			//DbgPrintA(",,,,,,,,,,,,,,,,,,,,,,,,,,,,hwid,,,,,,,,,,,,,,");
			//FillMemory(OutputBuffer, OutputBufferLength, 0);
			//OutputBufferLength = 0;
			//status = STATUS_ACCESS_DENIED;

			//*((ULONG*)OutputBuffer+10) = g_volnumer;
			//CopyMemory((ULONG*)OutputBuffer+10, g_hddsn, 8);

			ULONG len = OutputBufferLength;
			if (len>100) len=100;
			string str = String::to_hex_string((UCHAR*)OutputBuffer, len);
			//DbgPrintA("%s", str.c_str());
			DbgPrintA("%s, code:%p, %s", FunctionA, IoControlCode, str.c_str());
		}
	}
	else
	{
		if (OutputBufferLength)
		{
			ULONG len = OutputBufferLength;
			if (len>100) len=100;
			string str = String::to_hex_string((UCHAR*)OutputBuffer, len);
			//DbgPrintA("%s", str.c_str());
			DbgPrintA("%s, code:%p, %s", FunctionA, IoControlCode, str.c_str());
		}
	}

	return status;
}

ULONG ExecQuery;
HRESULT WINAPI ExecQuery_Hook(VOID* unk,int n2,int n3,int n4,int n5,int n6)
{
	VOID* caller = _caller();
	HRESULT hr;
	__asm {
		push n6
			push n5
			push n4
			push n3
			push n2
			push unk
			call ExecQuery
			mov hr, eax
	}
	DbgPrintW(L"%s,%p, hr:%p, %s,%s", FunctionW, caller, hr, n2,n3);
	return hr;
}

ULONG Get;
long WINAPI Get_Hook(VOID* unk,unsigned short const *n2,long n3,struct tagVARIANT *n4,long *n5,long *n6)
{
	VOID* caller = _caller();
	long hr;
	__asm {
		push n6
			push n5
			push n4
			push n3
			push n2
			push unk
			call Get
			mov hr, eax
	}
	DbgPrintW(L"%s,%p, hr:%p, %s,%p(bstrVal:%s, lVal:%p)", FunctionW, caller, hr, n2, n4, n4->bstrVal,n4->lVal);
	return hr;
}

ULONG fnAES_encrypt = (ULONG)AES_encrypt;
void CDECL fnAES_encrypt_Hook(const unsigned char *ain, unsigned char *aout,
				 const AES_KEY *key)
{
	void* caller = _caller();
	__asm {
		push key
			push aout
			push ain
			call fnAES_encrypt
			add esp, 0xc
	}
	string skey1 = String::to_hex_string(key->rd_key, 16);
	string skey2 = String::from_hex_string(skey1.c_str());
	DbgPrintA("%s, %p, in:%x, out:%x, key:%s", FunctionA, caller, *ain, *aout, skey2.c_str());
}

ULONG fnCRYPTO_cfb128_encrypt;
ULONG CDECL fnCRYPTO_cfb128_encrypt_Hook(int n1,int n2,int n3,int n4,int n5,int n6,int n7,int n8)
{
	void* caller = _caller();
	ULONG n;
	__asm {
		push n8
			push n7
			push n6
			push n5
			push n4
			push n3
			push n2
			push n1
			call fnCRYPTO_cfb128_encrypt
			mov n, eax
			add esp,0x20
	}
	string strin, strout, strkey;
	strin = String::to_hex_string((UCHAR*)n1, n3);
	strout = String::to_hex_string((UCHAR*)n2, n3);
	strkey = String::to_hex_string((UCHAR*)n4, 16);
	char szkey[20] = {0};
	lstrcpynA(szkey, (char*)n4, 17);
	DbgPrintA("%s, %p, %s,%s,%p,%s", FunctionA, caller, strin.c_str(), strout.c_str(), n3, szkey);
	return n;
}

BOOL CPropertyGridDlg::OnInitDialog()
{
	CPropertyGridDlgBase::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);         // Set big icon
	SetIcon(m_hIcon, FALSE);        // Set small icon

	//::SetUnhandledExceptionFilter(MyUnhandledExceptionFilter);
	//::AddVectoredExceptionHandler(1, VectoredHandler);

	//except1();

	DETOURS_Hook(fnAES_encrypt);
	HMODULE hlibeay = GetModuleHandle(TEXT("libeay32.dll"));
	if (hlibeay == NULL)
	{
		hlibeay = LoadLibrary(TEXT("libeay32.dll"));
	}
	fnCRYPTO_cfb128_encrypt = (ULONG)GetProcAddress(hlibeay, "CRYPTO_cfb128_encrypt");
	DbgPrintA("fnCRYPTO_cfb128_encrypt:%p", fnCRYPTO_cfb128_encrypt);
	DETOURS_Hook(fnCRYPTO_cfb128_encrypt);
	//DETOURS_Hook(_NtDeviceIoControlFile);

	ExecQuery = (ULONG)GetProcAddress(GetModuleHandle(TEXT("ole32.dll")), "ObjectStublessClient20");
	DbgPrintA("ObjectStublessClient20:%p", ExecQuery);
	DETOURS_Hook(ExecQuery);

	HMODULE hfastprox = GetModuleHandle(TEXT("fastprox.dll"));
	//DbgPrintA("hfastprox:%p", hfastprox);
	if (hfastprox == NULL)
	{
		hfastprox = LoadLibrary(TEXT("fastprox.dll"));
	}
	Get = (ULONG)GetProcAddress(hfastprox, "?Get@CWbemObject@@UAGJPBGJPAUtagVARIANT@@PAJ2@Z");
	DbgPrintA("hfastprox:%p, Get:%p", hfastprox, Get);
	DETOURS_Hook(Get);

	{
		COLORREF clr1[10], clr2[10];
		int rnd = 100;
		for (int i=0; i<10; i++)
		{
			clr1[i] = RGB(RandomUlong()%rnd, RandomUlong()%rnd, RandomUlong()%rnd);
			clr2[i] = RGB(RandomUlong()%rnd, RandomUlong()%rnd, RandomUlong()%rnd);
		}
		double fp = 0.85;
		BOOL b = Colors::IsSimilarColorArea(clr1, clr2, 10,1,0, fp);
		DbgPrint(TEXT("%s, IsSimilarColorArea:%d"), TFunction, b);
	}

	DbgPrint(TEXT("KiUserExceptionDispatcher:%p"), Undocument::KiUserExceptionDispatcher);
	
	{
		//f74188cba73d62ce0f6ca1eccab5debd869c5a2e7e3abaf86964dcb938763ff3
		string str = openssl::cipher::Base64::Decode("Y2Y2OTk5YjFlZDdiNzk5YTM2OTliM2ZhMTdkOTRhMmI5Y2RjYzkzYTAzODAwMWUyNmYxYzJhMDFmYmNiNDI5Mw==", false);
		DbgPrintA("key:%s", str.c_str());
		str = String::DecryptString(TextGetpath);
		DbgPrintA("TextSrv1:%s,%s", TextGetpath, str.c_str());

		openssl::cipher::md5 _md5;
		openssl::cipher::t_sha _sha;
		string strText = "123456";
		DbgPrintA("md5(%s)=%s", strText.c_str(), _md5.Md5Text(strText).c_str());
		DbgPrintA("sha(%s)=%s", strText.c_str(), _sha.ShaText(strText).c_str());

		//str = "CinnerMain";
		//for (size_t i=0; i<str.length()+1; i++)
		//{
		//	DbgPrintA("%s,%c", str.c_str(), str.c_str()[i]);
		//}
	}
	{
		//general load
		HMODULE h = LoadLibrary(TEXT("mfcdll.dll"));
		DbgPrintA("general load, h:%p,%p,%p", h,LoadLibrary(TEXT("05dll.dll")),LoadLibrary(TEXT("win32dll.dll")));

		////delete ldr
		//vector<TString> listM;
		//listM.push_back(TString(TEXT("dll.dll")));
		//listM.push_back(TString(TEXT("aa.dll")));
		//listM.push_back(TString(TEXT("ab.dll")));
		//listM.push_back(TString(TEXT("05dll.dll")));
		//listM.push_back(TString(TEXT("05mfcdll.dll")));
		//listM.push_back(TString(TEXT("win32dll.dll")));
		//ShellCode::RemoveLdrModuleList(listM);

		do 
		{
			h = (HMODULE)::GetModuleHandle(TEXT("mfcdll"));
			IMAGE_NT_HEADERS* nt = ShellCode::NtHeader(h);
			if (nt == NULL) break;
			DbgPrintA("h:%p, codesieze:%p, imagesize:%p", h, ShellCode::GetCodeSize(h), ShellCode::GetImageSize(h));

			for (int i=0; i<nt->FileHeader.NumberOfSections; i++)
			{
				IMAGE_SECTION_HEADER* sec = ShellCode::GetSection(h, i);
				if (sec == NULL) break;
				DbgPrintA("%d,%s,%p,%p", i, (CHAR*)sec->Name, sec->VirtualAddress+(UCHAR*)h, sec->Misc.VirtualSize);

				if (StrStrIA((CHAR*)sec->Name, ".data"))
				{
					prdata = sec->VirtualAddress+(UCHAR*)h;
					nrdata = sec->Misc.VirtualSize;
					DbgPrintA("%s:%s", (CHAR*)sec->Name, String::to_hex_string(prdata, 10, false, true).c_str());
				}
			}

			if (prdata)
			{
				HANDLE hthr = chBEGINTHREADEX(NULL, 0, Thread_Out, NULL, 0, NULL);
				CloseHandle(hthr);

				// hook NtReadVirtualMemo
				Old_NtReadVirtualMemory64.v = GetProcAddress64(g_hNtDll64, "NtReadVirtualMemory");
				DETOURS(TRUE, Old_NtReadVirtualMemory64.v, New_NtReadVirtualMemory64);
			}
		} while (0);

		////mmload
		//size_t nSize;
		//void* data = Process::ReadLibrary(TEXT("05dll.dll"), &nSize);
		//if (data)
		//{
		//	HMEMORYMODULE h2 = MemoryLoadLibrary(data, nSize);
		//	DbgPrintA("mmload, h:%p, data:%p, nSize:%p", h2, data, nSize);
		//}
	}
	{
		//DETOURS_Hook(_NtQueryInformationProcess);
		//DETOURS_Hook(_NtReadVirtualMemory);
		//DETOURS_Hook(_NtQueryVirtualMemory);
		//DETOURS_Hook(_NtCreateSection);
		//DETOURS_Hook(_NtMapViewOfSection);
		//DETOURS_Hook(_NtQueryInformationThread);
	}

	long addr = 0xffffffff;
	DbgPrintA("%p,%s", addr, String::to_hex_string((VOID*)addr, 11, false, true).c_str());


	//{
	//	PIMAGE_DOS_HEADER idh = (PIMAGE_DOS_HEADER)::GetModuleHandle(TEXT("PropertyGrid.exe"));
	//	PIMAGE_NT_HEADERS inh = (PIMAGE_NT_HEADERS)(idh->e_lfanew + (UCHAR*)idh);
	//	DbgPrint(TEXT("nt header:%p,NumberOfSections:%d, base:%p"),
	//		inh, inh->FileHeader.NumberOfSections, inh->OptionalHeader.ImageBase);

	//	for (int i=0; i<inh->FileHeader.NumberOfSections; i++)
	//	{
	//		PIMAGE_SECTION_HEADER ish = (PIMAGE_SECTION_HEADER)((UCHAR*)inh+sizeof(IMAGE_NT_HEADERS));
	//		ish += i;

	//		DbgPrintA("%p, %d, %s,%p,%p", ish, i, ish->Name, ish->VirtualAddress+inh->OptionalHeader.ImageBase, ish->Misc.VirtualSize);
	//		//break;
	//	}
	//}

	// 读xml设置
	m_set.fromFile(m_setfile);

	// create the property grid.
	if (m_wndPropertyGrid.Create(IDC_PLACEHOLDER, this))
	{
		m_wndPropertyGrid.SetVariableItemsHeight(TRUE);
		m_wndPropertyGrid.SetTheme(xtpGridThemeWhidbey);

		//////////////////////////////////////////////////////////////////////////
		// create document settings category.
		CXTPPropertyGridItem* pSettings = m_wndPropertyGrid.addGroup(IDS_MapleFunction);
		pSettings->SetTooltip(_T("Maple的一些功能"));

		CXTPPropertyGridItemBool* pItemBool = NULL;
		CXTPPropertyGridItem* pItem = NULL;
		CCustomItemSpin* pS = NULL;
		CCustomItemSlider* pSd = NULL;
		CCustomItemTime* pCit = NULL;

		pSettings->AddChildItem(new CXTPPropertyGridItem(IDS_zuobiao16,
			m_set.maple.strZuobiao16.valueStr(),
			&m_set.maple.strZuobiao16.m_value));
		pSettings->AddChildItem(new CXTPPropertyGridItem(IDS_zuobiao10,
			m_set.maple.strZuobiao10.valueStr(),
			&m_set.maple.strZuobiao10.m_value));
		
		pSettings->AddChildItem(new CCustomItemButton(IDS_zuobiao16to10));
		pSettings->AddChildItem(new CCustomItemButton(IDS_zuobiao10to16));

		pSettings->AddChildItem(new CCustomItemButton(IDS_MODULES));
		pSettings->AddChildItem(new CCustomItemButton(IDS_THREADREAD));
		pSettings->AddChildItem(new CCustomItemButton(IDS_NtStatusToDosError));

		pSettings->AddChildItem(new CCustomItemButton(IDS_test_getcolor));
		pSettings->AddChildItem(new CCustomItemButton(IDS_test_cmpcolor));

		pSettings = m_wndPropertyGrid.addGroup(IDS_yanzhengip);

		pSettings->AddChildItem(new CCustomItemProgressCtrl(IDS_saohaojindu)); //debug版会崩

		pSettings->AddChildItem(new CXTPPropertyGridItem(IDS_yanzhengiphaoshi));

		pSettings->AddChildItem(new CCustomItemFileBox(IDS_yanzhengipfile,
			m_set.yzip.yzipfile.valueStr(),
			&m_set.yzip.yzipfile.m_value, TEXT("文本"), TEXT("TXT Files (*.txt)||")));

		pSd = (CCustomItemSlider*)pSettings->AddChildItem(new CCustomItemSlider(IDS_yanzhengipthreadcnt,
			m_set.yzip.yzipthreadcnt.value(),
			&m_set.yzip.yzipthreadcnt.m_value));
		pSd->SetRange(1, 100);
		
		pSd = (CCustomItemSlider*)pSettings->AddChildItem(new CCustomItemSlider(IDS_yanzhengipfailretry,
			m_set.yzip.yzipretry.value(),
			&m_set.yzip.yzipretry.m_value));
		pSd->SetRange(0, 20);

		pItemBool = (CXTPPropertyGridItemBool*)pSettings->AddChildItem(new CXTPPropertyGridItemBool(IDS_yanzhengipstart,
			m_set.yzip.yzipstart.value(),
			&m_set.yzip.yzipstart.m_value));
		pItemBool->SetCheckBoxStyle(TRUE);

		pSettings = m_wndPropertyGrid.addGroup(IDS_DAOJISHI);
		pCit = (CCustomItemTime*)pSettings->AddChildItem(new CCustomItemTime(IDS_DAOJISHITIME));
		pSettings->AddChildItem(new CXTPPropertyGridItemNumber(IDS_DAOJISHICOUNT,
			m_set.countdown.daojishi.value(),
			&m_set.countdown.daojishi.m_value));
		pSettings->AddChildItem(new CCustomItemButton(IDS_DAOJISHISTART));
		pSettings->AddChildItem(new CCustomItemButton(IDS_DAOJISHISUSPEND));
		pSettings->AddChildItem(new CCustomItemButton(IDS_DAOJISHIRESUME));

		pSettings = m_wndPropertyGrid.addGroup(IDS_COUNTER);
		pSettings->AddChildItem(new CCustomItemCounter(IDS_COUNTERTIME));
		pSettings->AddChildItem(new CCustomItemButton(IDS_COUNTERADD));
		pSettings->AddChildItem(new CCustomItemButton(IDS_COUNTERSUB));

		pSettings = m_wndPropertyGrid.addGroup(IDS_EncryptText);
		pSettings->AddChildItem(new CCustomItemFileBox(IDS_AES_FILE,
			m_set.aesenc.aesFile.valueStr(),
			&m_set.aesenc.aesFile.m_value));
		pItem = pSettings->AddChildItem(new CXTPPropertyGridItem(IDS_AES_SHEETNAME));
		pItem->SetFlags(xtpGridItemHasComboButton /*| xtpGridItemHasEdit*/);
		pSettings->AddChildItem(new CXTPPropertyGridItem(IDS_AES_KEY,
			m_set.aesenc.aesKey.valueStr(),
			&m_set.aesenc.aesKey.m_value));
		pSettings->AddChildItem(new CCustomItemButton(IDS_AES_BTNENCRYPT));

		// add child items to category.
		//pItem = pSettings->AddChildItem(new CCustomItemListCtrl(IDS_ITEMLIST));
		//DbgPrint(TEXT("ItemList %p"), pItem);

		//pSettings->AddChildItem(new CCustomItemProgressCtrl(IDS_PROGRESS))->SetReadOnly(FALSE);

		//CXTPPropertyGridItemDate* pDate;
		//pDate = (CXTPPropertyGridItemDate*)pSettings->AddChildItem(new CXTPPropertyGridItemDate(IDS_TANXIANTIME,
		//	m_set.Doc.dtTanxian.value(),
		//	&m_set.Doc.dtTanxian.m_value));
		//DbgPrint(TEXT("%d,%d,%d %d,%d,%d"),
		//	m_set.Doc.dtTanxian.m_value.GetYear(),
		//	m_set.Doc.dtTanxian.m_value.GetMonth(),
		//	m_set.Doc.dtTanxian.m_value.GetDay(),
		//	m_set.Doc.dtTanxian.m_value.GetHour(),
		//	m_set.Doc.dtTanxian.m_value.GetMinute(),
		//	m_set.Doc.dtTanxian.m_value.GetSecond()
		//	);
		//pDate->SetDateFormat(_T("%m/%d/%Y %H:%M:%S"));
		//pDate->SetMask(_T(""), _T(""));

		//pItemBool = (CXTPPropertyGridItemBool*)pSettings->AddChildItem(new CXTPPropertyGridItemBool(IDS_SaveOnClose,
		//	m_set.Doc.SaveOnClose.value(),
		//	&m_set.Doc.SaveOnClose.m_value));
		//pItemBool->SetCheckBoxStyle(TRUE);

		//LOGFONT lf;
		//GetFont()->GetLogFont( &lf );
		//pSettings->AddChildItem(new CXTPPropertyGridItemFont(IDS_WindowFont, lf));

		//pSettings->AddChildItem(new CXTPPropertyGridItemSize(IDS_WindowSize,
		//	m_set.Doc.WindowSize.value(),
		//	&m_set.Doc.WindowSize.m_value
		//	));

		//CXTPPropertyGridItemMultilineString* pms;
		//pms = (CXTPPropertyGridItemMultilineString*)pSettings->AddChildItem(new CXTPPropertyGridItemMultilineString(IDS_MultiLine,
		//	m_set.Doc.MultiLine.valueStr(),
		//	&m_set.Doc.MultiLine.m_value));
		//TEXTMETRIC tm;
		//::GetTextMetrics(::GetDC(m_hWnd), &tm);
		//int nLine = 3;
		//pms->SetHeight(tm.tmHeight*nLine);
		//pms->SetMultiLinesCount(nLine);
		//pms->SetTooltip(TEXT("cmission\nlmission\ncmap"));
		//pms->SetReadOnly(TRUE);

		////////////////////////////////////////////////////////////////////////////
		//// create global settings category.
		//CXTPPropertyGridItem* pGlobals = m_wndPropertyGrid.AddCategory(IDS_GlobalSettings);

		//// add child items to category.
		//pGlobals->AddChildItem(new CXTPPropertyGridItem(IDS_Greeting,
		//	m_set.Glo.Greeting.valueStr(),
		//	&m_set.Glo.Greeting.m_value));

		//pGlobals->AddChildItem(new CXTPPropertyGridItemNumber(IDS_ItemsInMRUList,
		//	m_set.Glo.ItemsInMRUList.value(),
		//	&m_set.Glo.ItemsInMRUList.m_value));

		//pGlobals->AddChildItem(new CXTPPropertyGridItemNumber(IDS_MaxRepeatRate,
		//	m_set.Glo.MaxRepeatRate.value(),
		//	&m_set.Glo.MaxRepeatRate.m_value));

		//pGlobals->AddChildItem(new CXTPPropertyGridItemColor(IDS_ToolbarColor,
		//	m_set.Glo.ToolbarColor.value(),
		//	&m_set.Glo.ToolbarColor.m_value));

		////////////////////////////////////////////////////////////////////////////
		//// Version category.
		//CXTPPropertyGridItem* pVersion      = m_wndPropertyGrid.AddCategory(IDS_Version);

		//// add child items to category.
		//pVersion->AddChildItem(new CXTPPropertyGridItem(IDS_AppVersion,
		//	m_set.Ver.Version.valueStr(),
		//	&m_set.Ver.Version.m_value))->SetReadOnly(TRUE);

		//CXTPPropertyGridItem* pItemLanguage = pVersion->AddChildItem(new CXTPPropertyGridItem(ID_ITEM_VERSION_LANGUAGE,
		//	m_set.Ver.Lang.valueStr(),
		//	&m_set.Ver.Lang.m_value));
		//pItemLanguage->SetFlags(xtpGridItemHasComboButton /*| xtpGridItemHasEdit*/);
		//CXTPPropertyGridItemConstraints* pList = pItemLanguage->GetConstraints();
		//pList->AddConstraint(_T("Neutral"));
		//pList->AddConstraint(_T("Arabic"));
		//pList->AddConstraint(_T("German"));
		//pList->AddConstraint(_T("Chinese(Taiwan)"));
		//pList->AddConstraint(_T("English (United Kingdom)"));
		//pList->AddConstraint(_T("English (United States)"));
		//pList->AddConstraint(_T("France"));
		//pList->AddConstraint(_T("Russian"));

		////////////////////////////////////////////////////////////////////////////
		//// Dynamic Options
		//CXTPPropertyGridItem* pCategoryDynamic = m_wndPropertyGrid.AddCategory(IDS_DynamicOptions);

		//pItemBool = (CXTPPropertyGridItemBool*)pCategoryDynamic->AddChildItem(new CXTPPropertyGridItemBool(IDS_Advanced,
		//	m_set.Dyn.Advanced.value(),
		//	&m_set.Dyn.Advanced.m_value));
		//pItemBool->SetCheckBoxStyle();

		//pItemBool = (CXTPPropertyGridItemBool*)pCategoryDynamic->AddChildItem(new CXTPPropertyGridItemBool(IDS_Option1,
		//	m_set.Dyn.Option1.value(),
		//	&m_set.Dyn.Option1.m_value));
		//pItemBool->SetCheckBoxStyle();
		////pItemBool->SetHidden(TRUE);

		//pItemBool = (CXTPPropertyGridItemBool*)pCategoryDynamic->AddChildItem(new CXTPPropertyGridItemBool(IDS_Option2,
		//	m_set.Dyn.Option2.value(),
		//	&m_set.Dyn.Option2.m_value));
		//pItemBool->SetCheckBoxStyle();
		////pItemBool->SetHidden(TRUE);

		//pItemBool = (CXTPPropertyGridItemBool*)pCategoryDynamic->AddChildItem(new CXTPPropertyGridItemBool(IDS_Option3,
		//	m_set.Dyn.Option3.value(),
		//	&m_set.Dyn.Option3.m_value));
		//pItemBool->SetCheckBoxStyle();
		////pItemBool->SetHidden(TRUE);

		//pItemBool = (CXTPPropertyGridItemBool*)pCategoryDynamic->AddChildItem(new CXTPPropertyGridItemBool(IDS_Option4,
		//	m_set.Dyn.Option4.value(),
		//	&m_set.Dyn.Option4.m_value));
		//pItemBool->SetCheckBoxStyle();
		////pItemBool->SetHidden(TRUE);
		//pItemBool->SetReadOnly();

		////////////////////////////////////////////////////////////////////////////
		//// create standard items category.
		//CXTPPropertyGridItem* pStandard   = m_wndPropertyGrid.AddCategory(IDS_StandardItems);
		//pStandard->AddChildItem(new CXTPPropertyGridItem(IDS_Stringitem,
		//	m_set.Si.StrItem.valueStr(),
		//	&m_set.Si.StrItem.m_value));

		////pStandard->AddChildItem(new CXTPPropertyGridItemMultilineString(_T("Multiline String item"), _T("1\r\n2")));
		//pStandard->AddChildItem(new CXTPPropertyGridItemNumber(IDS_Integeritem,
		//	m_set.Si.IntItem.value(),
		//	&m_set.Si.IntItem.m_value));

		//pStandard->AddChildItem(new CXTPPropertyGridItemDouble(IDS_Doubleitem,
		//	m_set.Si.DbItem.value(),
		//	TEXT("%.2f"),
		//	&m_set.Si.DbItem.m_value));

		//pStandard->AddChildItem(new CXTPPropertyGridItemColor(IDS_Coloritem,
		//	m_set.Si.ClrItem.value(),
		//	&m_set.Si.ClrItem.m_value));

		//pStandard->AddChildItem(new CXTPPropertyGridItemBool(IDS_Boolitem,
		//	m_set.Si.BlItem.value(),
		//	&m_set.Si.BlItem.m_value));

		////pStandard->AddChildItem(new CXTPPropertyGridItemFont(_T("Font item"), lf));

		////COleDateTime dates(1980,2,13, 12,34,56);
		////CXTPPropertyGridItemDate* pDate;
		//pStandard->AddChildItem(new CXTPPropertyGridItemDate(IDS_Dateitem,
		//	m_set.Si.DTItem.value(),
		//	&m_set.Si.DTItem.m_value));
		////To use system format:
		////pDate->SetDateFormat(_T("%m/%d/%Y %H:%M:%S"));
		////pDate->SetMask(_T(""), _T(""));

		//pStandard->AddChildItem(new CXTPPropertyGridItemSize(IDS_Sizeitem,
		//	m_set.Si.SzItem.value(),
		//	&m_set.Si.SzItem.m_value));

		//pItem = pStandard->AddChildItem(new CXTPPropertyGridItemEnum(IDS_Enumitem,
		//	m_set.Si.EmItem.value(),
		//	(int*)&m_set.Si.EmItem.m_value));
		//pItem->GetConstraints()->AddConstraint(_T("Windows 98"), 1);
		//pItem->GetConstraints()->AddConstraint(_T("Windows 2000"), 2);
		//pItem->GetConstraints()->AddConstraint(_T("Windows XP"), 3);

		//pItem = pStandard->AddChildItem(new CXTPPropertyGridItemFlags(IDS_Flagitem,
		//	m_set.Si.FgItem.value(),
		//	(int*)&m_set.Si.FgItem.m_value));
		//pItem->GetConstraints()->AddConstraint(_T("All Windows"), 1+2+4+8+16);
		//pItem->GetConstraints()->AddConstraint(_T("Windows 98"), 1);
		//pItem->GetConstraints()->AddConstraint(_T("Windows 2000"), 2);
		//pItem->GetConstraints()->AddConstraint(_T("Windows XP"), 4);
		//pItem->GetConstraints()->AddConstraint(_T("Windows 7"), 8);
		//pItem->GetConstraints()->AddConstraint(_T("Windows 10"), 16);

		//CXTPPropertyGridItemOption* pItemOption; 
		//pItemOption= (CXTPPropertyGridItemOption*)pStandard->AddChildItem(new CXTPPropertyGridItemOption(IDS_Optionitem1,
		//	m_set.Si.OpItem1.value(),
		//	(int*)&m_set.Si.OpItem1.m_value));
		//pItemOption->GetConstraints()->AddConstraint(_T("Windows XP"), 1);
		//pItemOption->GetConstraints()->AddConstraint(_T("Windows 2000"), 2);
		//pItemOption->GetConstraints()->AddConstraint(_T("Windows 7"), 4);
		//pItemOption->SetCheckBoxStyle();

		//pItemOption = (CXTPPropertyGridItemOption*)pStandard->AddChildItem(new CXTPPropertyGridItemOption(IDS_Optionitem2,
		//	m_set.Si.OpItem2.value(),
		//	(int*)&m_set.Si.OpItem2.m_value));
		//pItemOption->GetConstraints()->AddConstraint(_T("Windows XP"), 1);
		//pItemOption->GetConstraints()->AddConstraint(_T("Windows 2000"), 2);
		//pItemOption->GetConstraints()->AddConstraint(_T("Windows 7"), 3);


		////////////////////////////////////////////////////////////////////////////
		//CXTPPropertyGridItem* pButtons   = m_wndPropertyGrid.AddCategory(IDS_StandardButtons);
		//pButtons->AddChildItem(new CXTPPropertyGridItemBool(IDS_ComboButton,
		//	m_set.Sb.Combo.value(),
		//	&m_set.Sb.Combo.m_value))->SetFlags(xtpGridItemHasComboButton);

		//pButtons->AddChildItem(new CXTPPropertyGridItem(IDS_ExpandButton,
		//	m_set.Sb.Expand.valueStr(),
		//	&m_set.Sb.Expand.m_value))->SetFlags(xtpGridItemHasEdit | xtpGridItemHasExpandButton);

		//pItem = pButtons->AddChildItem(new CXTPPropertyGridItem(IDS_2Buttons,
		//	m_set.Sb.twoBtn.valueStr(),
		//	&m_set.Sb.twoBtn.m_value));
		//pItem->SetFlags(xtpGridItemHasEdit | xtpGridItemHasComboButton | xtpGridItemHasExpandButton);
		//pItem->GetConstraints()->AddConstraint(_T("Windows 2000"), 1);
		//pItem->GetConstraints()->AddConstraint(_T("Windows 98"), 2);

		//CXTPPropertyGridInplaceButton* pButton = pButtons->AddChildItem(new CXTPPropertyGridItem(IDS_TextButton,
		//	m_set.Sb.TextBtn.valueStr(),
		//	&m_set.Sb.TextBtn.m_value))->GetInplaceButtons()->AddButton(new CXTPPropertyGridInplaceButton(IDS_TextButton));
		//pButton->SetCaption(_T("Find"));

		//pItem = pButtons->AddChildItem(new CXTPPropertyGridItem(IDS_ImageButton,
		//	m_set.Sb.ImgBtn.valueStr(),
		//	&m_set.Sb.ImgBtn.m_value));
		//pButton = pItem->GetInplaceButtons()->AddButton(new CXTPPropertyGridInplaceButton(IDS_ImageButton));
		//pButton->SetIconIndex(100);
		//UINT btnFilter[] = {100};
		//m_wndPropertyGrid.GetImageManager()->SetIcons(IDB_BITMAP_FILTER, btnFilter, 1, 0);
		//pButton->SetTooltip(_T("Set Filter for item"));

		//pItem = pButtons->AddChildItem(new CXTPPropertyGridItemNumber(IDS_SpinAndSlider,
		//	m_set.Sb.Slider.value(),
		//	&m_set.Sb.Slider.m_value));
		//pItem->AddSliderControl();
		//pItem->AddSpinButton();

		//pItem = pButtons->AddChildItem(new CXTPPropertyGridItem(IDS_HyperlinkButton,
		//	m_set.Sb.Hyperlink.valueStr(),
		//	&m_set.Sb.Hyperlink.m_value));
		////pItem->SetFlags(xtpGridItemHasExpandButton);
		//pButton = pItem->GetInplaceButtons()->AddButton(new CXTPPropertyGridInplaceButton(IDS_HyperlinkButton));
		//pButton->SetHyperlink();
		//pButton->SetShowAlways(TRUE);
		//pButton->SetAlignment(DT_LEFT);
		//pButton->SetCaption(_T("Click Me"));


		////////////////////////////////////////////////////////////////////////////
		//CXTPPropertyGridItem* pMetrics   = m_wndPropertyGrid.AddCategory(IDS_CustomMetrics);

		//pItem = pMetrics->AddChildItem(new CXTPPropertyGridItem(IDS_ValueColors,
		//	m_set.Cm.clrValue.valueStr(),
		//	&m_set.Cm.clrValue.m_value));
		//pItem->GetValueMetrics()->m_clrFore = RGB(255, 0, 0) ; //0xFF;
		//pItem->GetValueMetrics()->m_clrBack = RGB(235, 235, 235);

		//pItem = pMetrics->AddChildItem(new CXTPPropertyGridItem(IDS_CaptionColors,
		//	m_set.Cm.clrCaption.valueStr(),
		//	&m_set.Cm.clrCaption.m_value));
		//pItem->GetCaptionMetrics()->m_clrFore = 0xFF0000;
		//pItem->GetCaptionMetrics()->m_clrBack = RGB(235, 235, 235);

		//m_wndPropertyGrid.GetImageManager()->SetMaskColor(0xC0C0C0);
		//m_wndPropertyGrid.GetImageManager()->SetIcons(IDB_BITMAP_CONSTRAINTS, 0, 5, CSize(20, 14));
		//pItem = pMetrics->AddChildItem(new CXTPPropertyGridItemEnum(IDS_Images,
		//	m_set.Cm.emImages.value(),
		//	(int*)&m_set.Cm.emImages.m_value));
		//pItem->GetConstraints()->AddConstraint(_T("Green"), 0, 0);
		//pItem->GetConstraints()->AddConstraint(_T("Red"), 1, 1);
		//pItem->GetConstraints()->AddConstraint(_T("Yellow"), 2, 2);
		//pItem->GetConstraints()->AddConstraint(_T("Blue"), 3, 3);
		//pItem->GetValueMetrics()->m_nImage = m_set.Cm.emImages.m_value;
		//pItem->GetCaptionMetrics()->m_nImage = 4;

		//pItem = pMetrics->AddChildItem(new CXTPPropertyGridItem(IDS_VariableHeight,
		//	m_set.Cm.Height.valueStr(),
		//	&m_set.Cm.Height.m_value));
		//pItem->GetConstraints()->AddConstraint(_T("Item"), 1);
		//pItem->GetConstraints()->AddConstraint(_T("List"), 2);
		//pItem->GetConstraints()->AddConstraint(_T("View"), 3);
		//pItem->SetHeight(25);
		//pItem->SetFlags(xtpGridItemHasComboButton);

		////pItem = pMetrics->AddChildItem(new CXTPPropertyGridItem(_T("MultiLine"), _T("Codejock Software\r\n428 Corunna Avenue\r\nOwosso, Michigan 48867 USA")));
		////pItem->SetMultiLinesCount(3);

		////////////////////////////////////////////////////////////////////////////
		//// create custom items category.
		//CXTPPropertyGridItem* pCustom   = m_wndPropertyGrid.AddCategory(IDS_CustomItems);

		//// add child items to category.
		//pCustom->AddChildItem(new CCustomItemIcon(IDS_Icon,
		//	m_set.Ci.sIcon.valueStr(),
		//	&m_set.Ci.sIcon.m_value));

		//pCustom->AddChildItem(new CCustomItemRect(IDS_DockPadding,
		//	m_set.Ci.DockPadding.value(),
		//	&m_set.Ci.DockPadding.m_value));

		//pCustom->AddChildItem(new CCustomItemColor(IDS_CustomCombolist,
		//	m_set.Ci.Combolist.value(),
		//	&m_set.Ci.Combolist.m_value));

		//pCustom->AddChildItem(new CCustomItemFileBox(IDS_FileBox,
		//	m_set.Ci.Filebox.valueStr(),
		//	&m_set.Ci.Filebox.m_value));

		//pCustom->AddChildItem(new CXTPPropertyGridItem(IDS_Mask,
		//	m_set.Ci.Mask.valueStr(),
		//	&m_set.Ci.Mask.m_value))->SetMask(_T("(000) 000-0000-0000"), _T("(___) ___-____-____"));

		//pCustom->AddChildItem(new CXTPPropertyGridItem(IDS_Password,
		//	m_set.Ci.Password.valueStr(),
		//	&m_set.Ci.Password.m_value))->SetPasswordMask();

		//pCustom->AddChildItem(new CXTPPropertyGridItemDate(IDS_Date,
		//	m_set.Ci.Date.value(),
		//	&m_set.Ci.Date.m_value));

		//pCustom->AddChildItem(new CCustomItemUpperCase(IDS_UpperCase,
		//	m_set.Ci.Upper.valueStr(),
		//	&m_set.Ci.Upper.m_value));

		//pCustom->AddChildItem(new CCustomItemIPAddress(IDS_IPAddress,
		//	m_set.Ci.Ip.valueStr(),
		//	&m_set.Ci.Ip.m_value));

		//pCustom->AddChildItem(new CCustomItemMenu(IDS_PopupMenu,
		//	m_set.Ci.PopMenu.valueStr(),
		//	&m_set.Ci.PopMenu.m_value));

		////pCustom->AddChildItem(new CCustomItemEdit(_T("Output"), _T("Debug")));

		////// add multi level tree node.
		////CXTPPropertyGridItem* pCategoryOne    = pCustom->AddChildItem(new CXTPPropertyGridItemCategory(_T("First Sub Category")));
		////CXTPPropertyGridItem* pCategoryTwo    = pCategoryOne->AddChildItem(new CXTPPropertyGridItemCategory(_T("Second Sub Category 1")));
		////pCategoryTwo->AddChildItem(new CXTPPropertyGridItem(_T("Third Level 1"), _T("")));
		////pCategoryTwo->AddChildItem(new CXTPPropertyGridItem(_T("Third Level 2"), _T("")));
		////CXTPPropertyGridItem* pCategoryTwo2   = pCategoryOne->AddChildItem(new CXTPPropertyGridItemCategory(_T("Second Sub Category 2")));
		////pCategoryTwo2->AddChildItem(new CXTPPropertyGridItem(_T("Third Level 1"), _T("")));

		////CXTPPropertyGridItem* pItemOne    = pCustom->AddChildItem(new CXTPPropertyGridItem(_T("First Level"), _T("")));
		////CXTPPropertyGridItem* pItemTwo    = pItemOne->AddChildItem(new CXTPPropertyGridItem(_T("Second Level"), _T("")));
		////CXTPPropertyGridItem* pItemThird     = pItemTwo->AddChildItem(new CXTPPropertyGridItem(_T("Third Level"), _T("")));
		////pItemThird->AddChildItem(new CXTPPropertyGridItem(_T("Fourth Level 1"), _T("")));
		////pItemThird->AddChildItem(new CXTPPropertyGridItem(_T("Fourth Level 2"), _T("")));

		////////////////////////////////////////////////////////////////////////////
		//// create custom items category.
		//pCustom   = m_wndPropertyGrid.AddCategory(IDS_CustomButtons);

		//pCustom->AddChildItem(new CCustomItemSpin(IDS_SpinButton,
		//	m_set.Cb.Spin.value(),
		//	&m_set.Cb.Spin.m_value));

		//pCustom->AddChildItem(new CCustomItemSlider(IDS_Slider,
		//	m_set.Cb.Slider.value(),
		//	&m_set.Cb.Slider.m_value));

		//pItemBool = (CXTPPropertyGridItemBool*)pCustom->AddChildItem(new CXTPPropertyGridItemBool(IDS_Quanping,
		//	m_set.Cb.Quanping.value(),
		//	&m_set.Cb.Quanping.m_value));
		//pItemBool->SetCheckBoxStyle(TRUE);
		//
		//pItemBool = (CXTPPropertyGridItemBool*)pCustom->AddChildItem(new CXTPPropertyGridItemBool(IDS_Wuyanshi,
		//	m_set.Cb.Wuyanshi.value(),
		//	&m_set.Cb.Wuyanshi.m_value));
		//pItemBool->SetCheckBoxStyle(TRUE);

		////pCustom->AddChildItem(new CCustomItemCheckBox(IDS_Quanping,
		////	m_set.Cb.Quanping.valueStr(),
		////	&m_set.Cb.Quanping.m_value));

		////pCustom->AddChildItem(new CCustomItemCheckBox(IDS_Wuyanshi,
		////	m_set.Cb.Wuyanshi.valueStr(),
		////	&m_set.Cb.Wuyanshi.m_value));

		//pCustom->AddChildItem(new CCustomItemButton(IDS_LeftOrigin));

		//pCustom->AddChildItem(new CCustomItemButton(IDS_RightOrigin));

		//pCustom->AddChildItem(new CCustomItemButton(_T("Pointer"), TRUE, TRUE));
		//pCustom->AddChildItem(new CCustomItemButton(_T("Gradient"), TRUE, FALSE));

		//////////////////////////////////////////////////////////////////////////
		// 刷新所有设置值; 期间不保存到文件
		m_bInitUpdate = TRUE;
		m_wndPropertyGrid.UpdateGridItems(m_wndPropertyGrid.GetCategories());
		m_bInitUpdate = FALSE;
	}

	// Set control resizing.
	SetMinSize(CSize(0,0));
	SetResize(IDC_PLACEHOLDER, m_wndPropertyGrid.GetSafeHwnd(), CXTPResizeRect(0,0,1,1));
	//SetResize(IDC_BUTTON1, CXTPResizeRect(0,1,0,1));
	//SetResize(IDC_BUTTON2, CXTPResizeRect(0,1,0,1));
	//SetResize(IDC_BUTTON3, CXTPResizeRect(0,1,0,1));
	//SetResize(IDC_BUTTON4, CXTPResizeRect(0,1,0,1));
	//SetResize(IDC_LIST1, CXTPResizeRect(0,1,1,1));

	// Load window placement
	BOOL bLoad = AutoLoadPlacement(_T("PropertyGridSample"));
	if (bLoad == FALSE)
	{
		this->ShowWindow(SW_HIDE);
		this->MoveWindow(0, 0, 380, 500, TRUE);
		Controls::MoveWindowTo(this->m_hWnd, Controls::MW_RIGHTTOP);
		this->ShowWindow(SW_SHOW);
	}

	m_wndPropertyGrid.SendNotifyMessage(XTP_PGN_VERB_CLICK, (LPARAM)this);


	//CCustomItemListCtrl* pl = (CCustomItemListCtrl*)m_wndPropertyGrid.FindItem(IDS_ITEMLIST);
	//CListCtrl& list = pl->GetListCtrl();
	//list.InsertColumn(0, TEXT("aaa"), LVCFMT_LEFT, 50);
	//list.InsertColumn(1, TEXT("bbb"), LVCFMT_LEFT, 50);
	//list.InsertColumn(2, TEXT("ccc"), LVCFMT_LEFT, 50);

	////////////////////////////////////////////////////////////////////////////
	//SetTimer(1, 100, NULL);
	//SetTimer(2, 300, NULL);
	//SetTimer(5, 1000, NULL);
	//SetTimer(6, 3000, NULL);
	SetTimer(7, 3000, NULL);

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CPropertyGridDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CPropertyGridDlgBase::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CPropertyGridDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CPropertyGridDlgBase::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CPropertyGridDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

LRESULT CPropertyGridDlg::OnGridNotify(WPARAM wParam, LPARAM lParam)
{
	bool Updated = false;

	switch (wParam)
	{
	case XTP_PGN_VERB_CLICK:
		{
			if (m_bCollapse)
			{
				m_wndPropertyGrid.DoCollapseExpand(m_wndPropertyGrid.GetCategories(), &CXTPPropertyGridItem::Collapse);
				m_wndPropertyGrid.GetVerbs()->RemoveAll();
				m_wndPropertyGrid.GetVerbs()->Add(TEXT("全部展开"), IDS_GRIDVERBS);
			}
			else
			{
				m_wndPropertyGrid.DoCollapseExpand(m_wndPropertyGrid.GetCategories(), &CXTPPropertyGridItem::Expand);
				m_wndPropertyGrid.GetVerbs()->RemoveAll();
				m_wndPropertyGrid.GetVerbs()->Add(TEXT("全部收起"), IDS_GRIDVERBS);
			}
			m_bCollapse = !m_bCollapse;
		} break;

	//case XTP_PGN_INPLACEBUTTONDOWN:
	//	{
	//		CXTPPropertyGridInplaceButton* pButton = (CXTPPropertyGridInplaceButton*)lParam;
	//		DbgPrint(TEXT("InplaceButton click. Caption = %s, ID = %i"), pButton->GetCaption(), pButton->GetID());

	//		if (pButton->GetItem()->GetID() == 510 && pButton->GetID() == XTP_ID_PROPERTYGRID_EXPANDBUTTON) // 2 Buttons
	//		{
	//			AfxMessageBox(_T("Expand button"));
	//			return TRUE;
	//		}
	//	} break;

	case XTP_PGN_ITEMVALUE_CHANGED:
		{
			Updated = true;

			CXTPPropertyGridItem* pItem = (CXTPPropertyGridItem*)lParam;
			DbgPrint(_T("Value Changed:%p. Caption = %s, ID = %i, Value = %s\n"), pItem, pItem->GetCaption(), pItem->GetID(), pItem->GetValue());

			switch (pItem->GetID())
			{
				//////////////////////////////////////////////////////////////////////////
				// Maple 坐标转换
				HANDLE_ITEMVALUE_CHANGED(IDS_zuobiao16to10, Grid_OnZuobiao16to10);
				HANDLE_ITEMVALUE_CHANGED(IDS_zuobiao10to16, Grid_OnZuobiao10to16);

				HANDLE_ITEMVALUE_CHANGED(IDS_MODULES, Grid_OnModules);
				HANDLE_ITEMVALUE_CHANGED(IDS_THREADREAD, Grid_OnThreadRead);
				HANDLE_ITEMVALUE_CHANGED(IDS_NtStatusToDosError, Grid_OnNtStatusToDosError);

				HANDLE_ITEMVALUE_CHANGED(IDS_test_getcolor, Grid_GetColor);
				HANDLE_ITEMVALUE_CHANGED(IDS_test_cmpcolor, Grid_CmpColor);

				//////////////////////////////////////////////////////////////////////////
				// http ip yanzheng
				HANDLE_ITEMVALUE_CHANGED(IDS_yanzhengipfile, Grid_OnYanzhengipFile);
				HANDLE_ITEMVALUE_CHANGED(IDS_yanzhengipstart, Grid_OnYanzhengipStart);

				//////////////////////////////////////////////////////////////////////////
				// daojishi
				HANDLE_ITEMVALUE_CHANGED(IDS_DAOJISHICOUNT, Grid_OnDaojishi);
				HANDLE_ITEMVALUE_CHANGED(IDS_DAOJISHISTART, Grid_OnDaojishiStart);
				HANDLE_ITEMVALUE_CHANGED(IDS_DAOJISHISUSPEND, Grid_OnDaojishiSuspend);
				HANDLE_ITEMVALUE_CHANGED(IDS_DAOJISHIRESUME, Grid_OnDaojishiResume);
				// counter
				HANDLE_ITEMVALUE_CHANGED(IDS_COUNTERADD, Grid_OnCounterAdd);
				HANDLE_ITEMVALUE_CHANGED(IDS_COUNTERSUB, Grid_OnCounterSub);
				// aes
				HANDLE_ITEMVALUE_CHANGED(IDS_AES_FILE, Grid_OnAesFile);
				HANDLE_ITEMVALUE_CHANGED(IDS_AES_BTNENCRYPT, Grid_OnBtnAesEncrypt);
			}
		} break;

	//case XTP_PGN_SELECTION_CHANGED:
	//	{
	//		CXTPPropertyGridItem* pItem = (CXTPPropertyGridItem*)lParam;
	//		if (pItem)
	//		{
	//			if (pItem->IsKindOf(RUNTIME_CLASS(CXTPPropertyGridItemColor)))
	//			{
	//				//DbgPrint(_T("Color Item. Selection Changed. Item = %s\n"), pItem->GetCaption());
	//			}

	//			if (pItem->GetID() == ID_ITEM_VERSION_LANGUAGE)
	//			{
	//				//pItem->SetDescription(pItem->GetDescription() + "!");
	//			}
	//		}
	//	} break;

	//case XTP_PGN_EDIT_CHANGED:
	//	{
	//		Updated = true;

	//		CXTPPropertyGridInplaceEdit* pEdit = DYNAMIC_DOWNCAST(CXTPPropertyGridInplaceEdit, (CWnd*)lParam);
	//		if (pEdit && pEdit->GetItem())
	//		{
	//			// Custom Validation
	//			if (pEdit->GetItem()->GetID() == ID_ITEM_VERSION_LANGUAGE)
	//			{
	//				CString str;
	//				pEdit->CEdit::GetWindowText(str);

	//				if (str.GetLength() > 30)
	//				{
	//					MessageBeep((UINT)-1);
	//					pEdit->SetSel(0, -1);
	//					pEdit->ReplaceSel(str.Left(30));
	//				}
	//			}
	//			// Custom Validation
	//			if (pEdit->GetItem()->GetCaption() == _T("ItemsInMRUList"))
	//			{
	//				CString str;
	//				pEdit->CEdit::GetWindowText(str);

	//				int i = _ttoi(str);
	//				if (i > 20)
	//				{
	//					MessageBeep((UINT)-1);
	//					pEdit->SetSel(0, -1);
	//					pEdit->ReplaceSel(_T("20"));
	//				}
	//			}
	//		}
	//	} break;
	}

	if (Updated && m_bInitUpdate == FALSE)
	{
		//DbgPrint(TEXT("%s, %s,%s"), TFunction, m_set.yzip.yzipfile.valueStr(), m_set.yzip.yzipfile.m_value);
		m_set.toFile(m_setfile);
	}

	return 0;
}

void CPropertyGridDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	switch (nIDEvent)
	{
	case 1:
		{
			{
				CXTPPropertyGridItemNumber* p = (CXTPPropertyGridItemNumber*)m_wndPropertyGrid.FindItem(IDS_DAOJISHICOUNT);
				if (p)
				{
					CString str;
					str.Format(TEXT("%d"), m_set.countdown.daojishi.m_value);
					p->OnValueChanged(str);
					KillTimer(nIDEvent);
				}
			}
			//{
			//	CCustomItemProgressCtrl* p = (CCustomItemProgressCtrl*)m_wndPropertyGrid.FindItem(IDS_saohaojindu);
			//	if (p)
			//	{
			//		ULONG upp = RandomUlong()%10000;
			//		ULONG low = RandomUlong()%upp;
			//		p->SetPos(low*100/upp);
			//		CString str;
			//		str.Format(TEXT("經驗:%.2f%%"), (FLOAT)low/(FLOAT)upp*100.0);
			//		p->SetText(str);
			//		//str.Format(TEXT("%p;\r\n%p;\r\n%p;"), RandomUlong(), RandomUlong(), RandomUlong());
			//		//p->OnValueChanged(str);
			//	}
			//}
			
			//CCustomItemListCtrl* pl = (CCustomItemListCtrl*)m_wndPropertyGrid.FindItem(IDS_ITEMLIST);
			//if (pl)
			//{
			//	CListCtrl& list = pl->GetListCtrl();
			//	list.SetColumnWidth(0, 100);
			//	list.SetColumnWidth(1, 100);
			//	list.DeleteAllItems();
			//	int n = RandomUlong()%1+3;
			//	TCHAR buff[100];
			//	for (int i=0; i<n; i++)
			//	{
			//		StringCchPrintf(buff, 100, TEXT("aa%d"), i+1);
			//		list.InsertItem(i, buff);
			//		StringCchPrintf(buff, 100, TEXT("%p"), RandomUlong());
			//		list.SetItemText(i, 1, buff);
			//	}
			//	//list.InsertItem(list.GetItemCount(), TEXT("aa1"));
			//	////KillTimer(nIDEvent);
			//	//static int n = 0;
			//	//n++;
			//	//if (n % 2 == 0)
			//	//{
			//	//	//list.DeleteItem(0);
			//	//}
			//	//if (n % 10 == 0)
			//	//{
			//	//	list.DeleteAllItems();
			//	//	//KillTimer(nIDEvent);
			//	//}
			//}
		} break;

	case 2:
		{
			KillTimer(nIDEvent);
			if (m_set.yzip.yzipfile.m_value.GetLength())
			{
				CXTPPropertyGridItem* p = m_wndPropertyGrid.FindItem(IDS_yanzhengipfile);
				if (p)
				{
					p->OnValueChanged(m_set.yzip.yzipfile.m_value);
				}
			}
			if (m_set.aesenc.aesFile.m_value.GetLength())
			{
				CXTPPropertyGridItem* p = m_wndPropertyGrid.FindItem(IDS_AES_FILE);
				if (p)
				{
					p->OnValueChanged(m_set.aesenc.aesFile.m_value);
				}
			}
		} break;

	case 3:
		{
			static int n = 0;
			static COleDateTime t1, t2;

			if (n == 0)
			{
				t1 = COleDateTime::GetCurrentTime();
			}
			if (n++ == 3)
			{
				n = 0;
				KillTimer(nIDEvent);

				t2 = COleDateTime::GetCurrentTime();
				COleDateTimeSpan ts = t2 - t1;
				DbgPrint(TEXT("间隔:%d秒"), ts.GetSeconds());
			}
		} break;

	case 4:
		{
			COleDateTimeSpan ts = COleDateTime::GetCurrentTime() - m_dtStart;
			CString strTime = ts.Format(TEXT("%H:%M:%S"));
			CXTPPropertyGridItem* p = m_wndPropertyGrid.FindItem(IDS_yanzhengiphaoshi);
			if (p)
			{
				p->OnValueChanged(strTime);
			}
		} break;

	case 5:
		{
			KillTimer(nIDEvent);
			DWORD rd;

			HANDLE hMap = ::CreateFileMapping(INVALID_HANDLE_VALUE, NULL, PAGE_READWRITE, 0, nrdata, TEXT("dllfilemapping"));
			DbgPrintA("hMap:%p", hMap);

			UCHAR* pView = (UCHAR*)::MapViewOfFile(hMap, FILE_MAP_READ|FILE_MAP_WRITE, 0, 0, 0);
			CopyMemory(pView, prdata, nrdata);
			::UnmapViewOfFile(pView);

			//test open filemapping
			SYSTEM_INFO si;
			::GetSystemInfo(&si);
			DbgPrintA("systeminfo, %p,%p,%p", si.dwPageSize, si.lpMinimumApplicationAddress, si.lpMaximumApplicationAddress);

			HANDLE hMap2 = ::OpenFileMapping(FILE_MAP_READ, FALSE, TEXT("dllfilemapping"));
			DbgPrintA("OpenMap:%p", hMap2);

			pView = (UCHAR*)::MapViewOfFile(hMap2, FILE_MAP_READ, 0, 0, 0);
			DbgPrint(TEXT("%s"), Output::GetFormattedMessageString().c_str());

			UCHAR qsi[1*KB];
			MEMORY_BASIC_INFORMATION mbi;
			Undocument::NtQueryVirtualMemory(GetCurrentProcess(), pView, MemoryBasicInformation, &mbi, sizeof(mbi), &rd);
			//DbgPrintA("mbi, %s", String::to_hex_string(&mbi, rd).c_str());
			//DbgPrintA("	BaseAddress:%p", mbi.BaseAddress);
			//DbgPrintA("	AllocationBase:%p", mbi.AllocationBase);
			//DbgPrintA("	AllocationProtect:%p", mbi.AllocationProtect);
			//DbgPrintA("	RegionSize:%p", mbi.RegionSize);
			//DbgPrintA("	State:%p", mbi.State);
			//DbgPrintA("	Protect:%p", mbi.Protect);
			//DbgPrintA("	Type:%p", mbi.Type);

			DbgPrintA("MapView:%p,%s", pView, String::to_hex_string((UCHAR*)pView, 10, false, true).c_str());


			////MEMORY_WORKING_SET_INFORMATION mwsi;
			//Undocument::NtQueryVirtualMemory(GetCurrentProcess(), pView, MemoryWorkingSetInformation, &qsi, sizeof(qsi), &rd);
			//DbgPrintA("mwsi, %s", String::to_hex_string(&qsi, rd).c_str());

			//Undocument::NtQueryVirtualMemory(GetCurrentProcess(), pView, MemoryRegionInformation, &qsi, sizeof(qsi), &rd);
			//DbgPrintA("region, %s", String::to_hex_string(&qsi, rd).c_str());

			//Undocument::NtQueryVirtualMemory(GetCurrentProcess(), pView, MemoryWorkingSetExInformation, &qsi, sizeof(qsi), &rd);
			//DbgPrintA("workingsetex, %s", String::to_hex_string(&qsi, rd).c_str());

			//Undocument::NtQueryVirtualMemory(GetCurrentProcess(), pView, MemorySharedCommitInformation, &qsi, sizeof(qsi), &rd);
			//DbgPrintA("sharedcommit, %s", String::to_hex_string(&qsi, rd).c_str());

			//Undocument::NtQueryVirtualMemory(GetCurrentProcess(), pView, MemoryImageInformation, &qsi, sizeof(qsi), &rd);
			//DbgPrintA("imageinfo, %s", String::to_hex_string(&qsi, rd).c_str());

			//Undocument::NtQueryVirtualMemory(GetCurrentProcess(), pView, MemoryRegionInformationEx, &qsi, sizeof(qsi), &rd);
			//DbgPrintA("regionex, %s", String::to_hex_string(&qsi, rd).c_str());

			//Undocument::NtQueryVirtualMemory(GetCurrentProcess(), pView, MemoryPrivilegedBasicInformation, &qsi, sizeof(qsi), &rd);
			//DbgPrintA("privileged, %s", String::to_hex_string(&qsi, rd).c_str());

			//////////////////////////////////////////////////////////////////////////
			UnmapViewOfFile(pView);
			//
			CloseHandle(hMap2);
			CloseHandle(hMap);

			//call rvm
			UCHAR buff[100];
			::ReadProcessMemory(GetCurrentProcess(), (LPVOID)0x401234, buff, 10, &rd);
		} break;

	case 6:
		{
			DbgPrintA("timer6...");
			KillTimer(nIDEvent);

			do 
			{
				CStdioFile sf;
				CFileException fe;
				if (!sf.Open(TEXT("数字\\0.txt"), CFile::modeRead|CFile::typeText, &fe))
				{
					break;
				}
				CHAR bufA[400];
				CString rText;
				while (sf.ReadString(rText))
				{
					StringCchPrintfA(bufA, 400, "%S", rText);
					string s = bufA;
					s = String::from_hex_string(s, true);
					m_strshuzi[0].append(s);

				}
				sf.Close();

				DbgPrintA("0:%d(%p)\n%s", m_strshuzi[0].length(), m_strshuzi[0].length(),
					String::to_hex_string(m_strshuzi[0].c_str(), m_strshuzi[0].length()).c_str());
			} while (0);
			
			do 
			{
				CStdioFile sf;
				CFileException fe;
				if (!sf.Open(TEXT("数字\\1.txt"), CFile::modeRead|CFile::typeText, &fe))
				{
					break;
				}
				CHAR bufA[400];
				CString rText;
				while (sf.ReadString(rText))
				{
					StringCchPrintfA(bufA, 400, "%S", rText);
					string s = bufA;
					s = String::from_hex_string(s, true);
					m_strshuzi[1].append(s);

				}
				sf.Close();

				DbgPrintA("1:%d(%p)\n%s", m_strshuzi[1].length(), m_strshuzi[1].length(),
					String::to_hex_string(m_strshuzi[1].c_str(), m_strshuzi[1].length()).c_str());
			} while (0);
			
			do 
			{
				CStdioFile sf;
				CFileException fe;
				if (!sf.Open(TEXT("数字\\2.txt"), CFile::modeRead|CFile::typeText, &fe))
				{
					break;
				}
				CHAR bufA[400];
				CString rText;
				while (sf.ReadString(rText))
				{
					StringCchPrintfA(bufA, 400, "%S", rText);
					string s = bufA;
					s = String::from_hex_string(s, true);
					m_strshuzi[2].append(s);

				}
				sf.Close();

				DbgPrintA("2:%d(%p)\n%s", m_strshuzi[2].length(), m_strshuzi[2].length(),
					String::to_hex_string(m_strshuzi[2].c_str(), m_strshuzi[2].length()).c_str());
			} while (0);
			
			do 
			{
				CStdioFile sf;
				CFileException fe;
				if (!sf.Open(TEXT("数字\\3.txt"), CFile::modeRead|CFile::typeText, &fe))
				{
					break;
				}
				CHAR bufA[400];
				CString rText;
				while (sf.ReadString(rText))
				{
					StringCchPrintfA(bufA, 400, "%S", rText);
					string s = bufA;
					s = String::from_hex_string(s, true);
					m_strshuzi[3].append(s);

				}
				sf.Close();

				DbgPrintA("3:%d(%p)\n%s", m_strshuzi[3].length(), m_strshuzi[3].length(),
					String::to_hex_string(m_strshuzi[3].c_str(), m_strshuzi[3].length()).c_str());
			} while (0);
			
			do 
			{
				CStdioFile sf;
				CFileException fe;
				if (!sf.Open(TEXT("数字\\maohao.txt"), CFile::modeRead|CFile::typeText, &fe))
				{
					break;
				}
				CHAR bufA[400];
				CString rText;
				while (sf.ReadString(rText))
				{
					StringCchPrintfA(bufA, 400, "%S", rText);
					string s = bufA;
					s = String::from_hex_string(s, true);
					m_strshuzi[10].append(s);

				}
				sf.Close();

				DbgPrintA("maohao:%d(%p)\n%s", m_strshuzi[10].length(), m_strshuzi[10].length(),
					String::to_hex_string(m_strshuzi[10].c_str(), m_strshuzi[10].length()).c_str());
			} while (0);
		} break;

	case 7:
		{
			KillTimer(nIDEvent);
			////m_set.yzip.yzipthreadcnt = 11;
			////m_set.yzip.yzipretry = 5;
			////m_set.yzip.yzipstart = TRUE;
			//m_wndPropertyGrid.FindItem(IDS_yanzhengipthreadcnt)->OnValueChanged(CString(TEXT("11")));
			//m_wndPropertyGrid.FindItem(IDS_yanzhengipfailretry)->OnValueChanged(CString(TEXT("5")));
			//m_wndPropertyGrid.FindItem(IDS_yanzhengipstart)->OnValueChanged(CString(TEXT("True")));

			//m_wndPropertyGrid.Refresh();
		} break;
	}

	CXTPResizeDialog::OnTimer(nIDEvent);
}

//void CPropertyGridDlg::OnBnClickedButton1()
//{
//	// TODO: 在此添加控件通知处理程序代码
//	int n = m_list1.GetItemCount();
//	AppSettings::AccInfo* pai = m_set.Accinfo.newElement();
//	CString str;
//	openssl::cipher::t_sha sha;
//
//	str.Format(TEXT("%p"), RandomUlong());
//	pai->acc.AssignValue(str);
//	m_list1.InsertItem(n, str);
//
//	CHAR buff[100];
//	StringCchPrintfA(buff, 100, "%p", RandomUlong());
//	string strText = buff;
//	string strSha = sha.ShaText(strText);
//	pai->psw.AssignValue(strSha);
//	str.Format(TEXT("%S"), strSha.c_str());
//	m_list1.SetItemText(n, 1, str);
//
//	int data = n+1; //(int)RandomUlong();
//	pai->data.AssignValue(data);
//	str.Format(TEXT("%d"), data);
//	m_list1.SetItemText(n, 2, str);
//
//	m_set.toFile(m_setfile);
//}

//void CPropertyGridDlg::OnBnClickedButton2()
//{
//	// TODO: 在此添加控件通知处理程序代码
//	m_set.ClearCollection();
//	m_list1.DeleteAllItems();
//
//	m_set.toFile(m_setfile);
//}

//void CPropertyGridDlg::OnBnClickedButton3()
//{
//	// TODO: 在此添加控件通知处理程序代码
//	m_set.Doc.dtTanxian.m_value = COleDateTime::GetCurrentTime();
//	m_set.toFile(m_setfile);
//	//DbgPrint(TEXT("dtTanxian:%p"), m_set.Doc.dtTanxian.m_value);
//}

//void CPropertyGridDlg::OnBnClickedButton4()
//{
//	// TODO: 在此添加控件通知处理程序代码
//	COleDateTime t1 = m_set.Doc.dtTanxian.m_value;
//	DbgPrint(TEXT("t1,%d,%d,%d %d,%d,%d"),t1.GetYear(),t1.GetMonth(),t1.GetDay(),t1.GetHour(),t1.GetMinute(),t1.GetSecond());
//	COleDateTime t2 = COleDateTime::GetCurrentTime();
//	DbgPrint(TEXT("t2,%d,%d,%d %d,%d,%d"),t2.GetYear(),t2.GetMonth(),t2.GetDay(),t2.GetHour(),t2.GetMinute(),t2.GetSecond());
//	COleDateTimeSpan ts = t2 - t1;
//	DbgPrint(TEXT("間隔:%d天%d小时%d分%d秒"), ts.GetDays(),ts.GetHours(),ts.GetMinutes(),ts.GetSeconds());
//
//	CXTPPropertyGridItemNumber* p = (CXTPPropertyGridItemNumber*)m_wndPropertyGrid.FindItem(IDS_ItemsInMRUList);
//	//p->OnValueChanged(m_set.Doc.SaveOnClose.m_value? TEXT("False") : TEXT("True"));
//	p->Select();
//	Msg::FlashWindow(p->GetInplaceEdit().GetSafeHwnd());
//	//m_set.Glo.ItemsInMRUList.m_value = 99;
//	//m_set.toFile(m_setfile);
//	//m_wndPropertyGrid.Refresh();
//	//CString strValue = TEXT("99");
//	//p->OnValueChanged(strValue);
//
//	m_set.fromFile(m_setfile);
//	//DbgPrint(TEXT("1saveofclose:%d"), m_set.Doc.SaveOnClose.m_value);
//	//m_set.toFile(m_setfile);
//	//DbgPrint(TEXT("2saveofclose:%d"), m_set.Doc.SaveOnClose.m_value);
//	m_wndPropertyGrid.Refresh();
//	//DbgPrint(TEXT("3saveofclose:%d"), m_set.Doc.SaveOnClose.m_value);
//	//////////////////////////////////////////////////////////////////////////
//	// 刷新所有设置值; 期间不保存到文件
//	m_bInitUpdate = TRUE;
//	_UpdateGridItems(m_wndPropertyGrid.GetCategories());
//	m_bInitUpdate = FALSE;
//}

VOID CPropertyGridDlg::Grid_OnZuobiao16to10(CXTPPropertyGridItem* pItem)
{
	UNREFERENCED_PARAMETER(pItem);
	if (m_bInitUpdate) return;
	if (m_set.maple.strZuobiao16.m_value.GetLength() <= 0)
	{
		CXTPPropertyGridItem* p = (CXTPPropertyGridItem*)m_wndPropertyGrid.FindItem(IDS_zuobiao16);
		p->Select();
		Msg::FlashWindow(p->GetInplaceEdit().GetSafeHwnd());
		return;
	}
	CString str1;
	TCHAR buff[100];
	StringCchPrintf(buff, 100, TEXT("%s"), m_set.maple.strZuobiao16.m_value);
	str1 = buff;

	int x, y;
	x = Common::uShortFromString((LPTSTR)(LPCTSTR)str1.Mid(0,5));
	y = Common::uShortFromString((LPTSTR)(LPCTSTR)str1.Mid(6,11));
	str1.Format(TEXT("%d,%d"), x, y);

	CXTPPropertyGridItem* p = m_wndPropertyGrid.FindItem(IDS_zuobiao10);
	p->OnValueChanged(str1);
}

VOID CPropertyGridDlg::Grid_OnZuobiao10to16(CXTPPropertyGridItem* pItem)
{
	UNREFERENCED_PARAMETER(pItem);
	if (m_bInitUpdate) return;
	if (m_set.maple.strZuobiao10.m_value.GetLength() <= 0)
	{
		CXTPPropertyGridItem* p = (CXTPPropertyGridItem*)m_wndPropertyGrid.FindItem(IDS_zuobiao10);
		p->Select();
		Msg::FlashWindow(p->GetInplaceEdit().GetSafeHwnd());
		return;
	}
	CString str1;
	TCHAR buff[100];
	StringCchPrintf(buff, 100, TEXT("%s"), m_set.maple.strZuobiao10.m_value);
	str1 = buff;

	CStringArray sa;
	CHAR buff2[100] = {0};
	
	String::StrToK(sa, (LPTSTR)(LPCTSTR)str1, TEXT(", ，"));
	if (sa.GetCount() < 2) return;

	*(SHORT*)(buff2) = _ttoi(sa.GetAt(0));
	*(SHORT*)(buff2+2) = _ttoi(sa.GetAt(1));

	string s = String::to_hex_string(buff2, 4, true, true);
	str1.Format(TEXT("%S"), s.c_str());
	
	CXTPPropertyGridItem* p = m_wndPropertyGrid.FindItem(IDS_zuobiao16);
	p->OnValueChanged(str1);
}

void EnumModlueAll(DWORD dwPID)
{   
	HANDLE hProcess=OpenProcess(PROCESS_ALL_ACCESS,false,dwPID);
	if(hProcess==INVALID_HANDLE_VALUE)
	{
		DbgPrintA("open process failed!");
		return;
	}
	DWORD size=0,ret=0;
	EnumProcessModules(hProcess,NULL,size,&ret);
	HMODULE *parry=(HMODULE*)malloc(ret);
	memset(parry,0,ret);
	if(EnumProcessModules(hProcess,parry,ret,&ret))
	{
		TCHAR* path=new TCHAR[MAX_PATH];
		memset(path,0,MAX_PATH);
		UINT i=0;

		while(GetModuleFileNameEx(hProcess,parry[i],path,MAX_PATH))
		{
			DbgPrint(TEXT("方法1 模块%2d, %s"), ++i, path);
			memset(path,0,MAX_PATH);
		}
		delete path;

	}
	free(parry);

	CloseHandle(hProcess);
}

bool ForceLookUpModule(DWORD dwPID)
{
	HANDLE hProcess=OpenProcess(PROCESS_QUERY_INFORMATION,1,dwPID);
	if(hProcess==NULL)
		return FALSE;

	MEMORY_MAPPED_FILE_NAME_INFORMATION* Out_Data = (MEMORY_MAPPED_FILE_NAME_INFORMATION*)malloc(0x200u);
	DWORD retLength;
	WCHAR Path[MAX_PATH]={0};
	wchar_t wstr[MAX_PATH]={0};

	int n = 0;
	for(unsigned int i=0; i<0x7fffffff; i=i+0x10000)
	{ 
		if (NT_SUCCESS(Undocument::NtQueryVirtualMemory(hProcess,(PVOID)i,MemoryMappedFilenameInformation,Out_Data,512,&retLength)))
		{ 
			if(!IsBadReadPtr((BYTE*)Out_Data->Name.Buffer,1))
			{
				if(((BYTE*)Out_Data->Name.Buffer)[0]==0x5c)
				{
					if(wcscmp(wstr, Out_Data->Name.Buffer))			
					{   
						_wsetlocale(0,L"chs"); 				
						//GetUserPath(Out_Data->Name.Buffer);
						DbgPrintW(L"方法2 模块%2d, %s",++n, Out_Data->Name.Buffer);
					}
					StringCchCopyW(wstr, MAX_PATH-1, Out_Data->Name.Buffer);
				}
			}
		}
	}
	CloseHandle(hProcess);
	return TRUE;
}

BOOL GetProcessModule(DWORD dwPID)
{
	BOOL bRet    =    FALSE;
	BOOL bFound    =    FALSE;
	HANDLE hModuleSnap = NULL;
	MODULEENTRY32 me32 ={0};

	hModuleSnap = ::CreateToolhelp32Snapshot(TH32CS_SNAPMODULE,dwPID);//创建进程快照
	if(hModuleSnap == INVALID_HANDLE_VALUE)
	{   
		DbgPrint(TEXT("获取模块失败!\n"));
		return FALSE;
	}

	int n = 0;
	me32.dwSize = sizeof(MODULEENTRY32);
	if(::Module32First(hModuleSnap,&me32))//获得第一个模块
	{
		do{
			DbgPrint(TEXT("方法3 模块%2d, %s"),++n, me32.szExePath);
		}while(::Module32Next(hModuleSnap,&me32)); //调用NtMapViewOfSection
	}//递归枚举模块

	CloseHandle(hModuleSnap);
	return bFound;
}

#define RTL_DEBUG_QUERY_MODULES                             0x01
#define RTL_DEBUG_QUERY_BACKTRACES                          0x02
#define RTL_DEBUG_QUERY_HEAPS                               0x04
#define RTL_DEBUG_QUERY_HEAP_TAGS                           0x08
#define RTL_DEBUG_QUERY_HEAP_BLOCKS                         0x10
#define RTL_DEBUG_QUERY_LOCKS                               0x20

typedef struct _RTL_PROCESS_MODULE_INFORMATION
{
	ULONG Reserved[2];
	PVOID Base;
	ULONG Size;
	ULONG Flags;
	USHORT Index;
	USHORT Unknown;
	USHORT LoadCount;
	USHORT ModuleNameOffset;
	CHAR ImageName[256];
} RTL_PROCESS_MODULE_INFORMATION, *PRTL_PROCESS_MODULE_INFORMATION;

typedef struct _RTL_PROCESS_MODULES
{
	ULONG ModuleCount;
	RTL_PROCESS_MODULE_INFORMATION ModuleEntry[1];
} RTL_PROCESS_MODULES, *PRTL_PROCESS_MODULES;

typedef struct _RTL_PROCESS_HEAP_INFORMATION
{
	PVOID Base;
	ULONG Flags;
	USHORT Granularity;
	USHORT Unknown;
	ULONG Allocated;
	ULONG Committed;
	ULONG TagCount;
	ULONG BlockCount;
	ULONG Reserved[7];
	PVOID Tags;
	PVOID Blocks;
} RTL_PROCESS_HEAP_INFORMATION, *PRTL_PROCESS_HEAP_INFORMATION;

typedef struct _RTL_PROCESS_HEAPS
{
	ULONG HeapCount;
	RTL_PROCESS_HEAP_INFORMATION HeapEntry[1];
} RTL_PROCESS_HEAPS, *PRTL_PROCESS_HEAPS;

typedef struct _RTL_PROCESS_LOCK_INFORMATION
{
	PVOID Address;
	USHORT Type;
	USHORT CreatorBackTraceIndex;
	ULONG OwnerThreadId;
	ULONG ActiveCount;
	ULONG ContentionCount;
	ULONG EntryCount;
	ULONG RecursionCount;
	ULONG NumberOfSharedWaiters;
	ULONG NumberOfExclusiveWaiters;
} RTL_PROCESS_LOCK_INFORMATION, *PRTL_PROCESS_LOCK_INFORMATION;

typedef struct _RTL_PROCESS_LOCKS
{
	ULONG LockCount;
	RTL_PROCESS_LOCK_INFORMATION LockEntry[1];
} RTL_PROCESS_LOCKS, *PRTL_PROCESS_LOCKS;

typedef struct _RTL_PROCESS_BACKTRACE_INFORMATION
{
	PVOID SymbolicBackTrace;
	ULONG TraceCount;
	USHORT Index;
	USHORT Depth;
	PVOID BackTrace[16];
} RTL_PROCESS_BACKTRACE_INFORMATION, *PRTL_PROCESS_BACKTRACE_INFORMATION;

typedef struct _RTL_PROCESS_BACKTRACES
{
	ULONG CommittedMemory;
	ULONG ReservedMemory;
	ULONG NumberOfBackTraceLookups;
	ULONG NumberOfBackTraces;
	RTL_PROCESS_BACKTRACE_INFORMATION BackTraces[1];
} RTL_PROCESS_BACKTRACES, *PRTL_PROCESS_BACKTRACES;

typedef struct _RTL_DEBUG_BUFFER
{
	HANDLE SectionHandle;
	PVOID SectionBase;
	PVOID RemoteSectionBase;
	ULONG SectionBaseDelta;
	HANDLE EventPairHandle;
	ULONG Unknown[2];
	HANDLE RemoteThreadHandle;
	ULONG InfoClassMask;
	ULONG SizeOfInfo;
	ULONG AllocatedSize;
	ULONG SectionSize;
	PRTL_PROCESS_MODULES ModuleInformation;
	PRTL_PROCESS_BACKTRACES BackTraceInformation;
	PRTL_PROCESS_HEAPS HeapInformation;
	PRTL_PROCESS_LOCKS LockInformation;
	PVOID Reserved[8];
} RTL_DEBUG_BUFFER, *PRTL_DEBUG_BUFFER;

typedef PRTL_DEBUG_BUFFER (NTAPI *pfnRtlCreateQueryDebugBuffer)(
	IN ULONG Size,
	IN BOOLEAN EventPair);

typedef NTSTATUS (NTAPI *pfnRtlQueryProcessDebugInformation)(
	IN ULONG ProcessId,
	IN ULONG DebugInfoMask,
	IN OUT PRTL_DEBUG_BUFFER Buf);

typedef NTSTATUS (NTAPI *pfnRtlDestroyQueryDebugBuffer)(IN PRTL_DEBUG_BUFFER Buf);

void EnumModuleEx(DWORD dwPID)
{   
	NTSTATUS status;
	HMODULE hMod=GetModuleHandle(TEXT("ntdll.dll"));

	pfnRtlCreateQueryDebugBuffer RtlCreateQueryDebugBuffer =
		(pfnRtlCreateQueryDebugBuffer)GetProcAddress(hMod,"RtlCreateQueryDebugBuffer");

	pfnRtlQueryProcessDebugInformation RtlQueryProcessDebugInformation =
		(pfnRtlQueryProcessDebugInformation)GetProcAddress(hMod,"RtlQueryProcessDebugInformation");

	pfnRtlDestroyQueryDebugBuffer RtlDestroyQueryDebugBuffer =
		(pfnRtlDestroyQueryDebugBuffer)GetProcAddress(hMod,"RtlDestroyQueryDebugBuffer");

	if((hMod==NULL)||(RtlDestroyQueryDebugBuffer==NULL)||(RtlQueryProcessDebugInformation==NULL)||(RtlCreateQueryDebugBuffer==NULL))
	{
		DbgPrint(TEXT("函数定位失败！"));
		return;
	}	
	
	PRTL_DEBUG_BUFFER Buffer = RtlCreateQueryDebugBuffer(0, FALSE); //NtMapViewOfSection

	status = RtlQueryProcessDebugInformation(dwPID, RTL_DEBUG_QUERY_MODULES, Buffer);
	if (!NT_SUCCESS(status))
	{ 
		DbgPrint(TEXT("RtlQueryProcessDebugInformation函数调用失败，进程开了保护"));
		return;
	}


	RTL_PROCESS_MODULE_INFORMATION* ModuleInfo =(RTL_PROCESS_MODULE_INFORMATION*)(Buffer->ModuleInformation->ModuleEntry);

	ULONG count = Buffer->ModuleInformation->ModuleCount;
	for(ULONG i=0; i<count; i++)
	{
		DbgPrintA("方法4 模块%2d, %s", i+1, ModuleInfo->ImageName);
		ModuleInfo++;
	}
	
	RtlDestroyQueryDebugBuffer(Buffer);
}

void EnumSelfModule()
{
	void *PEB         = NULL,
		*Ldr         = NULL,
		*Flink       = NULL,
		*p           = NULL,
		*BaseAddress = NULL,
		*FullDllName = NULL;
	DbgPrintA("列举自身模块！\n");
	__asm
	{
		mov     eax,fs:[0x30]
		mov     PEB,eax
	}
	DbgPrintA( "PEB   = 0x%08X\n", PEB );
	Ldr   = *( ( void ** )( ( unsigned char * )PEB + 0x0c ) );
	DbgPrintA( "Ldr   = 0x%08X\n", Ldr );
	Flink = *( ( void ** )( ( unsigned char * )Ldr + 0x0c ) );
	DbgPrintA( "Flink = 0x%08X\n", Flink );

	int n = 0;
	p     = Flink;
	do
	{
		BaseAddress = *( ( void ** )( ( unsigned char * )p + 0x18 ) );
		FullDllName = *( ( void ** )( ( unsigned char * )p + 0x28 ) );
		//DbgPrintA( "p     = 0x%08X 0x%08X ", p, BaseAddress );
		DbgPrintW( L"方法5 模块%2d, %s", ++n, FullDllName );

		p = *( ( void ** )p );
	}
	while ( Flink != p );
	return;
}

bool IsValidModule(byte* i)
{   
	if(IsBadReadPtr((void*)i,sizeof(IMAGE_DOS_HEADER)))
		return false;

	IMAGE_DOS_HEADER *BasePoint=(IMAGE_DOS_HEADER *)i;

	PIMAGE_NT_HEADERS32 NtHead=(PIMAGE_NT_HEADERS32)(i+BasePoint->e_lfanew);
	if(IsBadReadPtr((void*)NtHead,PAGE_SIZE))
		return false;

	if((NtHead->FileHeader.Characteristics&IMAGE_FILE_DLL)==0)//过滤掉。exe文件
		return false;

	if(NtHead->OptionalHeader.Subsystem==0x2)
		return true;
	if(NtHead->OptionalHeader.Subsystem==0x3)
		return true;

	return false;
}
 
void Searchaa()
{   
	DbgPrintA("暴力搜索列举模块!\n");
	
	int Num=0;

	UCHAR* i=(PUCHAR)0x00500000;
	for(;i<(PUCHAR)0x7ffeffff;i+=PAGE_SIZE)
	{   
		if(IsValidModule(i))
		{
			DbgPrintA("方法6 模块%2d, %p",++Num, i);
		}	
		
	}
	DbgPrintA("\t\t total find module :%03d\n",Num);
}

void WINAPI GetPhyMacAddress(TCHAR* strServiceName)
{
	BOOL    bRet                = FALSE;
	// 网卡标识，xp下可以在注册表下对应位置找到，本例： 
	// HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion\NetworkCards\2\ServiceName 

	HANDLE hDev = CreateFile(TEXT("//./{EFB88E2F-5C33-4B4B-9BE0-DF6B5473E82A}"),
		GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, 0, 0);

	int inBuf;
	BYTE outBuf[256] = { 0 };
	DWORD BytesReturned;

	// 获取永久原生MAC地址  
	inBuf = OID_802_3_PERMANENT_ADDRESS;
	int a = IOCTL_NDIS_QUERY_GLOBAL_STATS;
	// 17<<16  | 0<<14  |  0<<2 | 2
	int b = CTL_CODE(FILE_DEVICE_PHYSICAL_NETCARD, 0, METHOD_OUT_DIRECT, FILE_ANY_ACCESS);
	DbgPrintA("iocode:%p, %p", a, b);

	// 12<<16 | 0<<14 | 0<<2 | f (3<<2|3)
	//FILE_DEVICE_NETWORK      METHOD_NEITHER

	if (DeviceIoControl(hDev, IOCTL_NDIS_QUERY_GLOBAL_STATS, (LPVOID)&inBuf, 4, outBuf, 256, &BytesReturned, NULL))
	{
		string str = String::to_hex_string(outBuf, BytesReturned);
		DbgPrintA(str.c_str());
		DbgPrint(TEXT("Real Mac Address: %02X-%02X-%02X-%02X-%02X-%02X"), outBuf[0], outBuf[1], outBuf[2], outBuf[3], outBuf[4], outBuf[5]);
	}

	// 获取当前MAC地址,可能被修改过  
	inBuf = OID_802_3_CURRENT_ADDRESS;
	if (DeviceIoControl(hDev, IOCTL_NDIS_QUERY_GLOBAL_STATS, (LPVOID)&inBuf, 4, outBuf, 256, &BytesReturned, NULL))
	{
		string str = String::to_hex_string(outBuf, BytesReturned);
		DbgPrintA(str.c_str());
		DbgPrint(TEXT("Current Mac Address: %02X-%02X-%02X-%02X-%02X-%02X"), outBuf[0], outBuf[1], outBuf[2], outBuf[3], outBuf[4], outBuf[5]);
	}

	DbgPrint(TEXT("%p,%p"), SMART_GET_VERSION,SMART_RCV_DRIVE_DATA);




	// 通过WMI获得硬盘序列号或其他信息
	// Step 1: --------------------------------------------------
	// Initialize COM. ------------------------------------------
	HRESULT hres;
	hres = CoInitializeEx(0, COINIT_MULTITHREADED);
	if (FAILED(hres))
	{
		DbgPrintA("Failed to initialize COM library. Error code = %p", hres);
		return;	// Program has failed.
	}

	// Step 2: --------------------------------------------------
	// Set general COM security levels --------------------------
	// Note: If you are using Windows 2000, you need to specify -
	// the default authentication credentials for a user by using
	// a SOLE_AUTHENTICATION_LIST structure in the pAuthList ----
	// parameter of CoInitializeSecurity ------------------------

	hres = CoInitializeSecurity(
		NULL,
		-1,	 // COM authentication
		NULL,	// Authentication services
		NULL,	 // Reserved
		RPC_C_AUTHN_LEVEL_DEFAULT,	// Default authentication
		RPC_C_IMP_LEVEL_IMPERSONATE,	// Default Impersonation?
		NULL,	// Authentication info
		EOAC_NONE,	// Additional capabilities
		NULL	// Reserved
		);

	if (FAILED(hres))
	{
		DbgPrintA("Failed to initialize security. Error code = %p", hres);
		CoUninitialize();
		return;	// Program has failed.
	}

	// Step 3: ---------------------------------------------------
	// Obtain the initial locator to WMI -------------------------

	IWbemLocator *pLoc = NULL;

	hres = CoCreateInstance(
		CLSID_WbemLocator,
		0,
		CLSCTX_INPROC_SERVER,
		IID_IWbemLocator, (LPVOID*)&pLoc);

	if (FAILED(hres))
	{
		DbgPrintA("Failed to create IWbemLocator object. Err code = %p", hres);
		CoUninitialize();
		return;	// Program has failed.
	}

	// Step 4: -----------------------------------------------------
	// Connect to WMI through the IWbemLocator::ConnectServer method

	IWbemServices *pSvc = NULL;

	// Connect to the root/cimv2 namespace with
	// the current user and obtain pointer pSvc
	// to make IWbemServices calls.
	hres = pLoc->ConnectServer(
		_bstr_t(L"ROOT\\CIMV2"),	// Object path of WMI namespace
		NULL,	// User name. NULL = current user
		NULL,	// User password. NULL = current
		0,	// Locale. NULL indicates current
		NULL,	// Security flags.
		0,	// Authority (e.g. Kerberos)
		0,	// Context object
		&pSvc	// pointer to IWbemServices proxy
		);

	if (FAILED(hres))
	{
		DbgPrintA("Could not connect. Error code = %p", hres);
		pLoc->Release();
		CoUninitialize();
		return;	// Program has failed.
	}

	DbgPrintA("Connected to ROOT//CIMV2 WMI namespace");


	// Step 5: --------------------------------------------------
	// Set security levels on the proxy -------------------------

	hres = CoSetProxyBlanket(
		pSvc,	// Indicates the proxy to set
		RPC_C_AUTHN_WINNT,	// RPC_C_AUTHN_xxx
		RPC_C_AUTHZ_NONE,	// RPC_C_AUTHZ_xxx
		NULL,	// Server principal name
		RPC_C_AUTHN_LEVEL_CALL,	// RPC_C_AUTHN_LEVEL_xxx
		RPC_C_IMP_LEVEL_IMPERSONATE,	// RPC_C_IMP_LEVEL_xxx
		NULL,	 // client identity
		EOAC_NONE	// proxy capabilities
		);

	if (FAILED(hres))
	{
		DbgPrintA("Could not set proxy blanket. Error code = %p", hres);
		pSvc->Release();
		pLoc->Release();
		CoUninitialize();
		return;	// Program has failed.
	}

	// Step 6: --------------------------------------------------
	// Use the IWbemServices pointer to make requests of WMI ----
	// For example, get the name of the operating system
	IEnumWbemClassObject* pEnumerator = NULL;

	BSTR strQuery = bstr_t("SELECT * FROM Win32_PhysicalMedia");
	//BSTR strQuery = bstr_t("SELECT * FROM Win32_DiskDrive WHERE (SerialNumber IS NOT NULL) AND (MediaType LIKE 'Fixed hard disk%')");
	//BSTR strQuery = bstr_t("SELECT MACAddress FROM Win32_NetworkAdapter WHERE NetEnabled = 'TRUE'");

	hres = pSvc->ExecQuery(
		bstr_t("WQL"),
		strQuery,
		WBEM_FLAG_FORWARD_ONLY | WBEM_FLAG_RETURN_IMMEDIATELY,
		NULL,
		&pEnumerator);

	if (FAILED(hres))
	{
		DbgPrintA("Query for operating system name failed. Error code = %p", hres);
		pSvc->Release();
		pLoc->Release();
		CoUninitialize();
		return;	// Program has failed.
	}

	// Step 7: -------------------------------------------------
	// Get the data from the query in step 6 -------------------

	IWbemClassObject *pclsObj;
	ULONG uReturn = 0;

	while (pEnumerator)
	{
		HRESULT hr = pEnumerator->Next(WBEM_INFINITE, 1,&pclsObj, &uReturn);

		if(0 == uReturn){
			break;
		}
		VARIANT vtProp;
		VariantInit(&vtProp);
		// Get the value of the Name property
		hr = pclsObj->Get(L"SerialNumber", 0, &vtProp, 0, 0);
		DbgPrintW(L"%s", vtProp.bstrVal);
		VariantClear(&vtProp);
	}

	// Cleanup
	// ========
	pSvc->Release();
	pLoc->Release();
	pEnumerator->Release();
	pclsObj->Release();
	CoUninitialize();




	//CString sFilePath;
	//sFilePath.Format(TEXT("\\\\.\\PHYSICALDRIVE%d"), 0);
	//HANDLE hFile=::CreateFile(sFilePath,
	//	GENERIC_READ | GENERIC_WRITE,
	//	FILE_SHARE_READ | FILE_SHARE_WRITE,
	//	NULL, OPEN_EXISTING,
	//	0, NULL);

	//DWORD dwBytesReturned = 0;
	//GETVERSIONINPARAMS gvopVersionParams;
	//DeviceIoControl(hFile,      //向设备对象发送SMART_GET_VERSION设备请求，获取硬盘属性
	//	SMART_GET_VERSION, //ntdevice.. 0x74080
	//	NULL,
	//	0,
	//	&gvopVersionParams,
	//	sizeof(gvopVersionParams),
	//	&dwBytesReturned, NULL);

	//DbgPrint(TEXT("%d,%d,%d"), gvopVersionParams.bVersion, gvopVersionParams.bRevision, gvopVersionParams.fCapabilities);

	//if(gvopVersionParams.bIDEDeviceMap <= 0)
	//	return bRet;

	////2。第二步，发送SMART_RCV_DRIVE_DATA设备请求，获取硬盘详细信息。
	//// IDE or ATAPI IDENTIFY cmd
	////int btIDCmd = 0;
	////SENDCMDINPARAMS InParams;
	////int nDrive =0;
	////btIDCmd = (gvopVersionParams.bIDEDeviceMap >> nDrive & 0x10) ? IDE_ATAPI_IDENTIFY : IDE_ATA_IDENTIFY;


	////// 输出参数
	////BYTE btIDOutCmd[sizeof(SENDCMDOUTPARAMS) + IDENTIFY_BUFFER_SIZE - 1];

	////if(DoIdentify(hFile,
	////	&InParams,
	////	(PSENDCMDOUTPARAMS)btIDOutCmd,
	////	(BYTE)btIDCmd,
	////	(BYTE)nDrive, &dwBytesReturned) == FALSE)    return -3;
	////::CloseHandle(hFile);

	////DWORD dwDiskData[256];
	////USHORT *pIDSector; // 对应结构IDSECTOR，见头文件

	////pIDSector = (USHORT*)((SENDCMDOUTPARAMS*)btIDOutCmd)->bBuffer;
	////for(int i=0; i < 256; i++)   dwDiskData[i] = pIDSector[i];

	////// 取系列号
	////ZeroMemory(szSerialNumber, sizeof(szSerialNumber));
	////strcpy(szSerialNumber, ConvertToString(dwDiskData, 10, 19));

	////// 取模型号
	////ZeroMemory(szModelNumber, sizeof(szModelNumber));
	////strcpy(szModelNumber, ConvertToString(dwDiskData, 27, 46));

	//CloseHandle(hFile);

	//return bRet;
}

VOID CPropertyGridDlg::Grid_OnModules(CXTPPropertyGridItem* pItem)
{
	UNREFERENCED_PARAMETER(pItem);
	if (m_bInitUpdate) return;
	
	CALLERNAME();
	//GetPhyMacAddress(TEXT("PHYSICALDRIVE0"));

	{
#define BUF_SIZE 256
		TCHAR szName[]=TEXT("Global\\MyFileMappingObject");
		TCHAR szMsg[]=TEXT("Message from first process");

		HANDLE hMapFile;
		LPCTSTR pBuf;

		hMapFile = CreateFileMapping(
			INVALID_HANDLE_VALUE,    // use paging file
			NULL,                    // default security 
			PAGE_READWRITE,          // read/write access
			0,                       // max. object size 
			BUF_SIZE,                // buffer size  
			szName);                 // name of mapping object

		DbgPrintA("hMapFile:%p", hMapFile);
		if (hMapFile == NULL)
		{
			DbgPrintA("Could not create file mapping object (%d).\n",
				GetLastError());
			return;
		}
		pBuf = (LPTSTR) MapViewOfFile(hMapFile,   // handle to map object
			FILE_MAP_ALL_ACCESS, // read/write permission
			0,
			0,
			BUF_SIZE);

		DbgPrintA("pBuf:%p", pBuf);
		if (pBuf == NULL)
		{
			DbgPrintA("Could not map view of file (%d).\n",
				GetLastError());
			return;
		}

		CopyMemory((PVOID)pBuf, szMsg, _tcslen(szMsg)*sizeof(TCHAR));

		UnmapViewOfFile(pBuf);

		CloseHandle(hMapFile);
	}

	//Process::SetPrivilege(TRUE);

	//EnumModlueAll(GetCurrentProcessId());
	//DbgPrint(TEXT("\n\n"));

	//ForceLookUpModule(GetCurrentProcessId());
	//DbgPrint(TEXT("\n\n"));

	//GetProcessModule(GetCurrentProcessId());
	//DbgPrint(TEXT("\n\n"));

	//EnumModuleEx(GetCurrentProcessId());
	//DbgPrint(TEXT("\n\n"));

	//EnumSelfModule();
	//DbgPrint(TEXT("\n\n"));

	//Search();
	//DbgPrint(TEXT("\n\n"));
}

UINT CALLBACK ThreadRead(LPVOID p)
{
	::Sleep(Common::RandomUlong()%1000+1000);
	ULONG* addr = (ULONG*)0x401000;
	DbgPrintA("%s, para:%p, tid[%d], read %p:%p", FunctionA, p, GetCurrentThreadId(), addr, *addr);
	return 0;
}
VOID CPropertyGridDlg::Grid_OnThreadRead(CXTPPropertyGridItem* pItem)
{
	UNREFERENCED_PARAMETER(pItem);
	if (m_bInitUpdate) return;

	for (int i=0; i<100; i++)
	{
		//DbgPrintA("%s, ThreadRead:%p", FunctionA, ThreadRead);
		HANDLE h = chBEGINTHREADEX(NULL, 0, ThreadRead, NULL, 0, NULL);
		CloseHandle(h);
		//::Sleep(100);
	}
}

DWORD ConvertNtStatusToWin32Error(LONG ntstatus)
{
    DWORD oldError;
    DWORD result;
    DWORD br;
    OVERLAPPED o;
    
    o.Internal = ntstatus;
    o.InternalHigh = 0;
    o.Offset = 0;
    o.OffsetHigh = 0;
    o.hEvent = 0;
    oldError = GetLastError();
    GetOverlappedResult(NULL, &o, &br, FALSE);
    result = GetLastError();
    SetLastError(oldError);
    return result;
}

VOID CPropertyGridDlg::Grid_OnNtStatusToDosError(CXTPPropertyGridItem* pItem)
{

	UNREFERENCED_PARAMETER(pItem);
	if (m_bInitUpdate) return;

	//try
	//{
	//	//////////////////////////////////////////////////////////////////////////
	//	// system error
	//	{
	//		CAdoLogger adoLogger;

	//		CString strDb = TEXT("SysErrMsg.mdb");
	//		adoLogger.CreateDb(strDb);
	//		adoLogger.OpenDb();

	//		CString strTable = TEXT("SysErr");
	//		CString strCreateTable = TEXT("CREATE TABLE SysErr (nError INTEGER PRIMARY KEY,ErrInfo Text(250));");
	//		adoLogger.CreateTable(strTable, strCreateTable);

	//		CTime tStart = CTime::GetCurrentTime();

	//		for (DWORD i=0; i<16000; i++)
	//		{
	//			TString tStrMsg = Output::GetFormattedMessageStringByErrorNo(i);
	//			//DbgPrint(TEXT("%d,%s(%d)"), i, tStrMsg.c_str(), tStrMsg.length());
	//			CString strErr = tStrMsg.c_str();
	//			//strErr.Replace(TEXT("\n"), TEXT("-"));
	//			strErr.Replace(TEXT("'"), TEXT("\""));
	//			DbgPrint(TEXT("%d,%s(%d)"), i, strErr, strErr.GetLength());

	//			CString strLog;
	//			strLog.Format(TEXT("INSERT INTO %s (nError,ErrInfo) VALUES (%d,'%s')"), adoLogger.m_strExpTable, i, strErr);
	//			//DbgPrint(TEXT("%s"), strLog);
	//			adoLogger.WriteLog(strLog);
	//		}
	//		adoLogger.CloseDb();

	//		CTime tEnd = CTime::GetCurrentTime();
	//		CTimeSpan tSpan = tEnd - tStart;
	//		DbgPrint(TEXT("time used:%d sec."), tSpan.GetTotalSeconds());
	//	}
	//	
	//	//////////////////////////////////////////////////////////////////////////
	//	// ntstatus to systemerror
	//	{
	//		CAdoLogger adoLogger(TString(TEXT("SysErrMsg.mdb")));

	//		adoLogger.OpenDb();

	//		CString strTable = TEXT("NtStatusToDosError");
	//		CString strCreateTable = TEXT("CREATE TABLE NtStatusToDosError (NtStatus TEXT(20) ,DosError INTEGER);");
	//		adoLogger.CreateTable(strTable, strCreateTable);

	//		for (NTSTATUS n=0; n<=0x1000; n++)
	//		{
	//			DWORD dw = Undocument::RtlNtStatusToDosError(n);
	//			DbgPrint(TEXT("n:%p, dw:%d"), n, dw);

	//			CString strLog;
	//			strLog.Format(TEXT("INSERT INTO %s (NtStatus,DosError) VALUES ('%p',%d)"), TEXT("NtStatusToDosError"), n, dw);
	//			//DbgPrint(TEXT("%s"), strLog);
	//			adoLogger.WriteLog(strLog);
	//		}
	//		for (NTSTATUS n=0x10000; n<=0x11000; n++)
	//		{
	//			DWORD dw = Undocument::RtlNtStatusToDosError(n);
	//			DbgPrint(TEXT("n:%p, dw:%d"), n, dw);

	//			CString strLog;
	//			strLog.Format(TEXT("INSERT INTO %s (NtStatus,DosError) VALUES ('%p',%d)"), TEXT("NtStatusToDosError"), n, dw);
	//			//DbgPrint(TEXT("%s"), strLog);
	//			adoLogger.WriteLog(strLog);
	//		}
	//		for (NTSTATUS n=0x40000000; n<=0x40001000; n++)
	//		{
	//			DWORD dw = Undocument::RtlNtStatusToDosError(n);
	//			DbgPrint(TEXT("n:%p, dw:%d"), n, dw);

	//			CString strLog;
	//			strLog.Format(TEXT("INSERT INTO %s (NtStatus,DosError) VALUES ('%p',%d)"), TEXT("NtStatusToDosError"), n, dw);
	//			//DbgPrint(TEXT("%s"), strLog);
	//			adoLogger.WriteLog(strLog);
	//		}
	//		for (NTSTATUS n=0x40010000; n<=0x40011000; n++)
	//		{
	//			DWORD dw = Undocument::RtlNtStatusToDosError(n);
	//			DbgPrint(TEXT("n:%p, dw:%d"), n, dw);

	//			CString strLog;
	//			strLog.Format(TEXT("INSERT INTO %s (NtStatus,DosError) VALUES ('%p',%d)"), TEXT("NtStatusToDosError"), n, dw);
	//			//DbgPrint(TEXT("%s"), strLog);
	//			adoLogger.WriteLog(strLog);
	//		}
	//		for (NTSTATUS n=0x80000000; n<=0x80001000; n++)
	//		{
	//			DWORD dw = Undocument::RtlNtStatusToDosError(n);
	//			DbgPrint(TEXT("n:%p, dw:%d"), n, dw);

	//			CString strLog;
	//			strLog.Format(TEXT("INSERT INTO %s (NtStatus,DosError) VALUES ('%p',%d)"), TEXT("NtStatusToDosError"), n, dw);
	//			//DbgPrint(TEXT("%s"), strLog);
	//			adoLogger.WriteLog(strLog);
	//		}
	//		for (NTSTATUS n=0x80010000; n<=0x80011000; n++)
	//		{
	//			DWORD dw = Undocument::RtlNtStatusToDosError(n);
	//			DbgPrint(TEXT("n:%p, dw:%d"), n, dw);

	//			CString strLog;
	//			strLog.Format(TEXT("INSERT INTO %s (NtStatus,DosError) VALUES ('%p',%d)"), TEXT("NtStatusToDosError"), n, dw);
	//			//DbgPrint(TEXT("%s"), strLog);
	//			adoLogger.WriteLog(strLog);
	//		}
	//		for (NTSTATUS n=0xc0000000; n<=0xc0001000; n++)
	//		{
	//			DWORD dw = Undocument::RtlNtStatusToDosError(n);
	//			DbgPrint(TEXT("n:%p, dw:%d"), n, dw);

	//			CString strLog;
	//			strLog.Format(TEXT("INSERT INTO %s (NtStatus,DosError) VALUES ('%p',%d)"), TEXT("NtStatusToDosError"), n, dw);
	//			//DbgPrint(TEXT("%s"), strLog);
	//			adoLogger.WriteLog(strLog);
	//		}
	//		for (NTSTATUS n=0xc0150000; n<=0xc0151000; n++)
	//		{
	//			DWORD dw = Undocument::RtlNtStatusToDosError(n);
	//			DbgPrint(TEXT("n:%p, dw:%d"), n, dw);

	//			CString strLog;
	//			strLog.Format(TEXT("INSERT INTO %s (NtStatus,DosError) VALUES ('%p',%d)"), TEXT("NtStatusToDosError"), n, dw);
	//			//DbgPrint(TEXT("%s"), strLog);
	//			adoLogger.WriteLog(strLog);
	//		}
	//		adoLogger.CloseDb();
	//	}

	//} CATCH_COM();
}

VOID CPropertyGridDlg::Grid_GetColor(CXTPPropertyGridItem* pItem)
{
	UNREFERENCED_PARAMETER(pItem);
	if (m_bInitUpdate) return;

	// 欢乐麻将全集，取游戏豆
	HWND h = ::FindWindow(TEXT("UnityWndClass"), TEXT("欢乐麻将全集")); //(HWND)0x00010282;
	DbgPrint(TEXT("h:%p"), h);
	CClientDC dc(CWnd::FromHandle(h));

	do 
	{
		CStdioFile sf;
		CFileException fe;
		if (!sf.Open(TEXT("colors.dat"), CFile::modeCreate|CFile::modeReadWrite|CFile::typeBinary, &fe))
		{
			TCHAR buf[MAX_PATH];
			fe.GetErrorMessage(buf, MAX_PATH);
			AfxMessageBox(buf);
			break;
		}
		for (int x=325; x<=384; x++)
		{
			for (int y=55; y<=74; y++)
			{
				COLORREF clr = dc.GetPixel(x, y);
				DbgPrintA("%d,%d %d,%d,%d", x, y,
					GetRValue(clr), GetGValue(clr), GetBValue(clr));
				sf.Write(&clr, sizeof(COLORREF));
			}
		}
		sf.Close();
	} while (0);
}

VOID CPropertyGridDlg::Grid_CmpColor(CXTPPropertyGridItem* pItem)
{
	UNREFERENCED_PARAMETER(pItem);
	if (m_bInitUpdate) return;

	HWND h = (HWND)0x00010282;
	CClientDC dc(CWnd::FromHandle(h));

	COLORREF* p = NULL;
	COLORREF* pHead = NULL;

	do 
	{
		p = (COLORREF*)malloc(91*24*sizeof(COLORREF));
		DbgPrintA("p:%p", p);
		if (p == NULL)
		{
			DbgPrintA("malloc error.");
			break;
		}
		pHead = p;

		for (int x=10; x<=100; x++)
		{
			for (int y=23; y<=46; y++)
			{
				COLORREF clr = dc.GetPixel(x, y);
				*p = clr;
				//DbgPrintA("%p,%p, %d,%d %d,%d,%d", p, *p, x, y,
				//	GetRValue(clr), GetGValue(clr), GetBValue(clr));
				p++;
			}
		}

		for (int i=0; i<91; i++)
		{
			BOOL bSimilar;
			double fper;

			// check 0
			fper = 0.82;
			bSimilar = Colors::IsSimilarColorArea(pHead+i*24, (COLORREF*)m_strshuzi[0].c_str(), m_strshuzi[0].length()/sizeof(COLORREF),24,0, fper);
			if (bSimilar)
			{
				DbgPrintA("%d, check 0, %d", i, bSimilar);
				i += (m_strshuzi[0].length()/4/24 -1);
				continue;
			}
			
			// check 1
			fper = 0.82;
			bSimilar = Colors::IsSimilarColorArea(pHead+i*24, (COLORREF*)m_strshuzi[1].c_str(), m_strshuzi[1].length()/sizeof(COLORREF),24,0,  fper);
			if (bSimilar)
			{
				DbgPrintA("%d, check 1, %d", i, bSimilar);
				i += (m_strshuzi[1].length()/4/24 -1);
				continue;
			}
			
			// check 2
			fper = 0.82;
			bSimilar = Colors::IsSimilarColorArea(pHead+i*24, (COLORREF*)m_strshuzi[2].c_str(), m_strshuzi[2].length()/sizeof(COLORREF),24,0,  fper);
			if (bSimilar)
			{
				DbgPrintA("%d, check 2, %d", i, bSimilar);
				i += (m_strshuzi[2].length()/4/24 -1);
				continue;
			}
			
			// check 3
			fper = 0.82;
			bSimilar = Colors::IsSimilarColorArea(pHead+i*24, (COLORREF*)m_strshuzi[3].c_str(), m_strshuzi[3].length()/sizeof(COLORREF),24,0,  fper);
			if (bSimilar)
			{
				DbgPrintA("%d, check 3, %d", i, bSimilar);
				i += (m_strshuzi[3].length()/4/24 -1);
				continue;
			}
			
			// check maohao
			fper = 0.82;
			bSimilar = Colors::IsSimilarColorArea(pHead+i*24, (COLORREF*)m_strshuzi[10].c_str(), m_strshuzi[10].length()/sizeof(COLORREF),24,0,  fper);
			if (bSimilar)
			{
				DbgPrintA("%d, check ':', %d", i, bSimilar);
				i += (m_strshuzi[10].length()/4/24 -1);
				continue;
			}
		}
	} while (0);

	free(p);
}

VOID CPropertyGridDlg::Grid_OnYanzhengipFile(CXTPPropertyGridItem* pItem)
{
	UNREFERENCED_PARAMETER(pItem);
	if (m_bInitUpdate) return;
	if (m_set.yzip.yzipfile.m_value.GetLength() > 0)
	{
		do 
		{
			CStdioFile sf;
			CFileException fe;
			if (!sf.Open(m_set.yzip.yzipfile.m_value, CFile::modeRead|CFile::typeText, &fe))
			{
				TCHAR szError[1024];
				fe.GetErrorMessage(szError, 1024);
				DbgPrint(TEXT("%s"), szError);
				break;
			}

			// ip地址
			//boost::wregex reg(L"((25[0-5]|2[0-4]\d|1\d{2}|[1-9]?\d)\.){3}(25[0-5]|2[0-4]\d|1\d{2}|[1-9]?\d):\d+");
			////boost::wsmatch what;
			//wstring wstr;

			CString strText;
			CStringArray sa;
			int i=0;
			while (sf.ReadString(strText))
			{
				i++;
				strText.TrimLeft();
				strText.TrimRight();

				if (strText.Left(2) == TEXT("//"))
				{
					DbgPrint(TEXT("第%d行注释"), i);
					continue;
				}

				sa.RemoveAll();
				//DbgPrint(TEXT("sf.ReadString %d:%s:%d"), i, strText, strText.GetLength());

				if (strText.GetLength())
				{
					String::StrToK(sa, (LPTSTR)(LPCTSTR)strText, TEXT(":"));
					if (sa.GetSize() == 2)
					{
						// regex chexk
						//wstr = strText;
						//if (!boost::regex_match(wstr, reg))
						//{
						//	DbgPrint(TEXT("第%d行正则不匹配"), i);
						//	continue;
						//}

						if (sa.GetAt(0).GetLength() < 20)
						{
							//DbgPrint(TEXT("%d:%s:%s"), i, sa.GetAt(0), sa.GetAt(1));
							Yanzhenip yip;
							yip.strip = sa.GetAt(0);
							yip.strport = sa.GetAt(1);
							m_listYanzhengip.AddTail(yip);
						}
					}
				}
			}

			DbgPrint(TEXT("list ip:%d"), m_listYanzhengip.GetCount());

			if (sf.m_hFile != CFile::hFileNull)
			{
				sf.Close();
			}

		} while (0);
	}
}

VOID CPropertyGridDlg::Grid_OnYanzhengipStart(CXTPPropertyGridItem* pItem)
{
	UNREFERENCED_PARAMETER(pItem);
	if (m_bInitUpdate) return;
	CXTPPropertyGridItemBool *pb = (CXTPPropertyGridItemBool*)m_wndPropertyGrid.FindItem(IDS_yanzhengipstart);
	if (pb)
	{
		DbgPrintA("%s,%d, %d,%d", FunctionA, pb->GetBool(), m_set.yzip.yzipthreadcnt.m_value, m_set.yzip.yzipretry.m_value);
		if (pb->GetBool())
		{
			m_wndPropertyGrid.SetReadOnlyGroup(IDS_yanzhengip, TRUE, IDS_yanzhengipstart);

			m_bThreadEndLoop = FALSE;

			m_dtStart = COleDateTime::GetCurrentTime();
			SetTimer(4, 1000, NULL);

			m_nHandlingCnt = 0;
			m_nHandledCnt = 0;
			m_nHttpOkCnt = 0;

			int nThreadSetCnt = m_set.yzip.yzipthreadcnt.m_value;
			for (int i=0; i<nThreadSetCnt; i++)
			{
				::QueueUserWorkItem(ThreadPool_Yanzhengip,
					(LPVOID)this,
					WT_EXECUTELONGFUNCTION);
			}
		}
		else
		{
			m_wndPropertyGrid.SetReadOnlyGroup(IDS_yanzhengip, FALSE, 0);

			m_bThreadEndLoop = TRUE;
			KillTimer(4);
		}
	}
}

VOID CPropertyGridDlg::Grid_OnDaojishi(CXTPPropertyGridItem* pItem)
{
	UNREFERENCED_PARAMETER(pItem);
	if (m_bInitUpdate) return;
	CCustomItemTime* p = (CCustomItemTime*)m_wndPropertyGrid.FindItem(IDS_DAOJISHITIME);
	if (p)
	{
		p->SetCountDown(m_set.countdown.daojishi.m_value);
	}
}
VOID cdok()
{
	CALLERNAME();
}
VOID CPropertyGridDlg::Grid_OnDaojishiStart(CXTPPropertyGridItem* pItem)
{
	UNREFERENCED_PARAMETER(pItem);
	if (m_bInitUpdate) return;
	CCustomItemTime* p = (CCustomItemTime*)m_wndPropertyGrid.FindItem(IDS_DAOJISHITIME);
	if (p)
	{
		p->SetCountDown(m_set.countdown.daojishi.m_value);
		p->Start(cdok);
	}
}

VOID CPropertyGridDlg::Grid_OnDaojishiSuspend(CXTPPropertyGridItem* pItem)
{
	UNREFERENCED_PARAMETER(pItem);
	if (m_bInitUpdate) return;
	CCustomItemTime* p = (CCustomItemTime*)m_wndPropertyGrid.FindItem(IDS_DAOJISHITIME);
	if (p)
	{
		p->Suspend();
	}
}
VOID CPropertyGridDlg::Grid_OnDaojishiResume(CXTPPropertyGridItem* pItem)
{
	UNREFERENCED_PARAMETER(pItem);
	if (m_bInitUpdate) return;
	CCustomItemTime* p = (CCustomItemTime*)m_wndPropertyGrid.FindItem(IDS_DAOJISHITIME);
	if (p)
	{
		p->Resume();
	}
}

int pos = 0;
VOID CPropertyGridDlg::Grid_OnCounterAdd(CXTPPropertyGridItem* pItem)
{
	UNREFERENCED_PARAMETER(pItem);
	if (m_bInitUpdate) return;
	CCustomItemCounter* p = (CCustomItemCounter*)m_wndPropertyGrid.FindItem(IDS_COUNTERTIME);
	if (p)
	{
		pos = p->SetPos(pos+1);
	}
}
VOID CPropertyGridDlg::Grid_OnCounterSub(CXTPPropertyGridItem* pItem)
{
	UNREFERENCED_PARAMETER(pItem);
	if (m_bInitUpdate) return;
	CCustomItemCounter* p = (CCustomItemCounter*)m_wndPropertyGrid.FindItem(IDS_COUNTERTIME);
	if (p)
	{
		pos = p->SetPos(pos-1);
	}
}

VOID CPropertyGridDlg::Grid_OnAesFile(CXTPPropertyGridItem* pItem)
{
	UNREFERENCED_PARAMETER(pItem);
	if (m_bInitUpdate) return;

	CXTPPropertyGridItem* pSheetName = m_wndPropertyGrid.FindItem(IDS_AES_SHEETNAME);
	CXTPPropertyGridItemConstraints* pList;

	pList = pSheetName->GetConstraints();
	pList->RemoveAll();

	CExcelEx excel(m_set.aesenc.aesFile.m_value);
	long nSheetCount = excel.GetSheetCount();
	//DbgPrint(TEXT("SheetCount:%d"), nSheetCount);
	for (long i=1; i<=nSheetCount; i++)
	{
		CString strSheetName = excel.GetSheetName(i);
		//DbgPrint(TEXT("sheet %d,%s"), i, strSheetName);
		pList->AddConstraint(strSheetName);
	}

	pList->SetCurrent(0);
	pSheetName->OnValueChanged(pList->GetAt(0));
}

VOID CPropertyGridDlg::Grid_OnBtnAesEncrypt(CXTPPropertyGridItem* pItem)
{
	UNREFERENCED_PARAMETER(pItem);
	if (m_bInitUpdate) return;
	if (m_set.aesenc.aesFile.m_value.GetLength() <= 0)
	{
		CXTPPropertyGridItem* p = m_wndPropertyGrid.FindItem(IDS_AES_FILE);
		Msg::FlashWindow(p);
		return;
	}
	if (m_set.aesenc.aesKey.m_value.GetLength() <= 0)
	{
		CXTPPropertyGridItem* p = m_wndPropertyGrid.FindItem(IDS_AES_KEY);
		Msg::FlashWindow(p);
		return;
	}
	
	CXTPPropertyGridItem* pSheetName = m_wndPropertyGrid.FindItem(IDS_AES_SHEETNAME);
	CXTPPropertyGridItemConstraints* pList;

	pList = pSheetName->GetConstraints();
	CString strSheetName = pSheetName->GetValue();
	DbgPrint(TEXT("Sheet,%s"), strSheetName);
	if (strSheetName.GetLength() <= 0)
	{
		CXTPPropertyGridItem* p = m_wndPropertyGrid.FindItem(IDS_AES_SHEETNAME);
		Msg::FlashWindow(p);
		return;
	}

	CExcelEx excel(m_set.aesenc.aesFile.m_value);
	excel.SetSheet(strSheetName);

	DbgPrint(TEXT("%s, [%d,%d]"), excel.m_worksheet_.GetName(), excel.GetRows(), excel.GetColumns());

	do 
	{
		CStdioFile sf;
		CFileException fe;
		if (!sf.Open(TEXT("EncryString.h"), CFile::modeCreate|CFile::modeReadWrite|CFile::typeText, &fe))
		{
			TCHAR szErr[MAX_PATH] = {0};
			fe.GetErrorMessage(szErr, MAX_PATH);
			DbgPrint(szErr);
			break;
		}
		setlocale( LC_CTYPE,"CHS"); 
		sf.WriteString(TEXT("#pragma once\n\n\n"));

		for (long iRow=2; iRow<=excel.GetRows(); iRow++)
		{
			//取名称
			CString strItemName;
			excel.GetItem(iRow, 1, strItemName);
			if (strItemName.GetLength() <= 0) continue;

			//取字符串
			CString strItemStr;
			excel.GetItem(iRow, 2, strItemStr);
			if (strItemStr.GetLength() <= 0) continue;

			//DbgPrint(TEXT("%d, %s,%s"), iRow, strItemName, strItemStr);
			CString str;
			str.Format(TEXT("// \"%s\"\n"), strItemStr);
			sf.WriteString(str);
			
			string strCell;
			wstring wstr = strItemStr;
			String::wstring2string(wstr, strCell);

			openssl::cipher::aes Aes;
			string strKey;
			wstring wstrKey = m_set.aesenc.aesKey.m_value;
			String::wstring2string(wstrKey, strKey);
			string strAesEnc = Aes.EncryptText_cfb(strCell, strKey, AES_ENCRYPT);
			
			CHAR buffA[MAX_PATH] = {0};
			StringCchPrintfA(buffA, MAX_PATH, "#define %S \"%s\"\n\n", strItemName, strAesEnc.c_str());
			strCell = buffA;
			sf.Write(strCell.c_str(), strCell.length());
		}

		sf.Close();
	} while (0);

	//wstring wstrfile = m_set.aesenc.aesFile.m_value;
	//string strfile;
	//String::wstring2string(wstrfile, strfile);

	//wstring wstrsheet = strSheetName;
	//string strsheet;
	//String::wstring2string(wstrsheet, strsheet);

	//YExcel::BasicExcel e;
	//bool b = e.Load(strfile.c_str());
	//YExcel::BasicExcelWorksheet* sheet1 = e.GetWorksheet(strsheet.c_str());
	//DbgPrintA("Sheet1:%p", sheet1);
	//if (sheet1)
	//{
	//	CStdioFile sf;
	//	CFileException fe;
	//	if (!sf.Open(TEXT("EncryString.h"), CFile::modeCreate|CFile::modeReadWrite|CFile::typeText, &fe))
	//	{
	//		TCHAR szErr[MAX_PATH] = {0};
	//		fe.GetErrorMessage(szErr, MAX_PATH);
	//		DbgPrint(szErr);
	//		return;
	//	}
	//	setlocale( LC_CTYPE,"chs"); 
	//	sf.WriteString(TEXT("#pragma once\n\n"));

	//	size_t maxRows = sheet1->GetTotalRows();
	//	size_t maxCols = sheet1->GetTotalCols();
	//	DbgPrintA("Dimension of %s (%d, %d)", sheet1->GetAnsiSheetName(), maxRows, maxCols);

	//	//for (size_t c=0; c<maxCols; ++c)
	//	//	DbgStrA("%10d", c+1);

	//	for (size_t r=1; r<maxRows; ++r)
	//	{
	//		DbgPrintA("%10d", r+1);
	//		CHAR buffA[MAX_PATH] = {0};
	//		WCHAR buffW[MAX_PATH] = {0};

	//		for (size_t c=0; c<maxCols; ++c)
	//		{
	//			YExcel::BasicExcelCell* cell = sheet1->Cell(r,c);
	//			switch (cell->Type())
	//			{
	//			case YExcel::BasicExcelCell::UNDEFINED:
	//				//DbgPrintA("-UNDEFINED-");
	//				break;

	//			case YExcel::BasicExcelCell::INT:
	//				DbgPrintA("%d", cell->GetInteger());
	//				break;

	//			case YExcel::BasicExcelCell::DOUBLE:
	//				DbgPrintA("%lf", cell->GetDouble());
	//				break;

	//			case YExcel::BasicExcelCell::STRING:
	//				{
	//					string strCell = cell->GetString();
	//					DbgPrintA("%d,STRING,%s", c, strCell.c_str());
	//					if (c == 0)
	//					{
	//						StringCchPrintfA(buffA, MAX_PATH, "#define %s", strCell.c_str());
	//					}
	//					if (c == 1)
	//					{
	//						CHAR tmp[MAX_PATH];
	//						StringCchPrintfA(tmp, MAX_PATH, "\n// \"%s\"\n", strCell.c_str());
	//						string stmp = tmp;
	//						wstring wtmp;
	//						String::string2wstring(stmp, wtmp);
	//						sf.WriteString(wtmp.c_str());

	//						openssl::cipher::aes Aes;
	//						string strKey;
	//						wstring wstrKey = m_set.aesenc.aesKey.m_value;
	//						String::wstring2string(wstrKey, strKey);
	//						string strAesEnc = Aes.EncryptText_cfb(strCell, strKey, AES_ENCRYPT);

	//						CHAR szTmp[MAX_PATH];
	//						StringCchPrintfA(szTmp, MAX_PATH, " \"%s\"\n", strAesEnc.c_str());
	//						StringCchCatA(buffA, MAX_PATH, szTmp);
	//						stmp = buffA;
	//						//DbgPrintA("stmp:%s", stmp.c_str());
	//						//String::string2wstring(stmp, wtmp);
	//						//sf.WriteString(wtmp.c_str());
	//						sf.Write(stmp.c_str(), stmp.length());
	//					}
	//				} break;

	//			case YExcel::BasicExcelCell::WSTRING:
	//				{
	//					wstring strCell = cell->GetWString();
	//					DbgPrintW(L"%d,WSTRING,%s", c, strCell.c_str());
	//					if (c == 0)
	//					{
	//						//StringCchPrintfA(buffA, MAX_PATH, "#define %s", strCell.c_str());
	//					}
	//					if (c == 1)
	//					{
	//						string stmp;
	//						wstring wtmp;
	//						StringCchPrintfW(buffW, MAX_PATH, L"\n// \"%s\"\n", strCell.c_str());
	//						//OutputDebugStringW(buffW);
	//						wtmp = buffW;
	//						sf.WriteString(wtmp.c_str());
	//						
	//						wtmp = strCell.c_str();
	//						String::wstring2string(wtmp, stmp);

	//						openssl::cipher::aes Aes;
	//						string strKey;
	//						wstring wstrKey = m_set.aesenc.aesKey.m_value;
	//						String::wstring2string(wstrKey, strKey);
	//						string strAesEnc = Aes.EncryptText_cfb(stmp, strKey, AES_ENCRYPT);
	//						
	//						CHAR szTmp[MAX_PATH];
	//						StringCchPrintfA(szTmp, MAX_PATH, " \"%s\"\n", strAesEnc.c_str());
	//						StringCchCatA(buffA, MAX_PATH, szTmp);
	//						stmp = buffA;
	//						//DbgPrintA("stmp:%s", stmp.c_str());
	//						//String::string2wstring(stmp, wtmp);
	//						//sf.WriteString(wtmp.c_str());
	//						sf.Write(stmp.c_str(), stmp.length());
	//					}
	//				} break;
	//			}
	//		}
	//	}

	//	sf.Close();
	//}
}

DWORD CPropertyGridDlg::ThreadPool_Yanzhengip(LPVOID p)
{
	CPropertyGridDlg *dlg = (CPropertyGridDlg*)AfxGetApp()->m_pMainWnd;
	//DbgPrintA("%s, dlg:%p", FunctionA, dlg);

	int n = ::InterlockedIncrement(&dlg->m_nHandlingCnt)-1;
	//DbgPrintA("%s,%d, n:%d,m_nHandlingCnt:%d",FunctionA,
	//	GetCurrentThreadId(), n, dlg->m_nHandlingCnt);

	try
	{
		POSITION pos = dlg->m_listYanzhengip.FindIndex(n);
		if (pos)
		{
			Yanzhenip yip = dlg->m_listYanzhengip.GetAt(pos);

			////////////////////////////////////////////////////////////////////////////
			CStringW strProxy;
			strProxy.Format(L"%s:%s", yip.strip.c_str(), yip.strport.c_str());
			wstring wstrProxy = strProxy;
			DbgPrintW(L"[%u]%d proxy[%s]", GetCurrentThreadId(), n, wstrProxy.c_str());

			CrackedUrl crackedUrl;
			CStringW url;
			string str;

			crackedUrl.SetProxy(wstrProxy);

			url.Format(L"http://ip.dnsexit.com/"); //查ip

			crackedUrl.setUrl(url);

			int ntry = 0;
			do 
			{
				if (dlg->m_bThreadEndLoop) break;

				crackedUrl.SendRequest();
				str = crackedUrl.Response();
				//str = CodePage::UTF8ToANSI(str);
				DbgPrintA("[%u]response ---\n%d,%s\n--- response", GetCurrentThreadId(), str.length(), str.c_str());

				if (str.length() > 10 && str.length() < 20) //ok
				{
					::InterlockedIncrement(&dlg->m_nHttpOkCnt);

					// write file
					dlg->m_csWriteIpFile.Lock();

					do 
					{
						CStdioFile sf;
						CFileException fe;

						CString strFile = dlg->m_set.yzip.yzipfile.m_value;
						CString s1 = strFile.Mid(strFile.ReverseFind(TEXT('.')));
						CString s2;
						s2.Format(TEXT("_ip可用%s"), s1);
						strFile.Replace(s1, s2);

						if (!sf.Open(strFile, CFile::modeCreate|CFile::modeWrite|CFile::typeText|CFile::modeNoTruncate, &fe))
						{
							TCHAR szError[1024];
							fe.GetErrorMessage(szError, 1024);
							DbgPrint(TEXT("%s"), szError);
							break;
						}
						sf.SeekToEnd();

						s1.Format(TEXT("%s:%s\n"), yip.strip.c_str(), yip.strport.c_str());
						sf.WriteString(s1);

						sf.Close();
					} while (0);

					dlg->m_csWriteIpFile.Unlock();

					break;
				}
				else if (str.length() > 50)
				{
					break;
				}
			} while (ntry++ < dlg->m_set.yzip.yzipretry.m_value); // 失败重连

			if (dlg->m_bThreadEndLoop) goto _ret;

			//////////////////////////////////////////////////////////////////////////
			// 进度
			{
				::InterlockedIncrement(&dlg->m_nHandledCnt);

				CCustomItemProgressCtrl* p = (CCustomItemProgressCtrl*)dlg->m_wndPropertyGrid.FindItem(IDS_saohaojindu);
				if (p)
				{
					if (dlg->m_listYanzhengip.GetCount())
					{
						p->SetPos((int)((dlg->m_nHandledCnt)*100/dlg->m_listYanzhengip.GetCount()));

						CString str;
						str.Format(TEXT("%d/%d (%d)"),
							dlg->m_nHandledCnt,
							dlg->m_listYanzhengip.GetCount(),
							dlg->m_nHttpOkCnt);

						p->SetText(str);
					}
				}
			}

			if (dlg->m_bThreadEndLoop) goto _ret;

			{
				//////////////////////////////////////////////////////////////////////////
				// 取消开始的勾
				if (dlg->m_nHandledCnt >= dlg->m_listYanzhengip.GetCount())
				{
					CXTPPropertyGridItemBool *pb = (CXTPPropertyGridItemBool*)dlg->m_wndPropertyGrid.FindItem(IDS_yanzhengipstart);
					if (pb)
					{
						pb->OnValueChanged(TEXT("False"));
					}

					MessageBeep(-1);
				}
				else
				{
					//////////////////////////////////////////////////////////////////////////
					// 开始下一个
					::QueueUserWorkItem(ThreadPool_Yanzhengip,
						(LPVOID)dlg,
						WT_EXECUTELONGFUNCTION);
				}
			}
		}
	}
	catch (CMemoryException* e)
	{
		TCHAR szError[MAX_PATH] = {0};
		e->GetErrorMessage(szError, MAX_PATH-1);
		DbgPrint(TEXT("[%u]catch CMemoryException:%s"), GetCurrentThreadId(), szError);
	}
	catch (CFileException* e)
	{
		TCHAR szError[MAX_PATH] = {0};
		e->GetErrorMessage(szError, MAX_PATH-1);
		DbgPrint(TEXT("[%u]catch CFileException:%s"), GetCurrentThreadId(), szError);
	}
	catch (CException* e)
	{
		TCHAR szError[MAX_PATH] = {0};
		e->GetErrorMessage(szError, MAX_PATH-1);
		DbgPrint(TEXT("[%u]catch CException:%s"), GetCurrentThreadId(), szError);
	}
	catch (...)
	{
		DbgPrint(TEXT("[%u]catch ...:%s"), GetCurrentThreadId(), GetFormattedMessageString().c_str());
	}

_ret:
	//DbgPrintA("%s[%u], end", FunctionA, GetCurrentThreadId());
	
	return 0;
}


VOID CPropertyGridDlg::UpdateSheetName(void)
{
	//if(m_appExcel.m_lpDispatch)
	//{
	//	m_appExcel.ReleaseDispatch();
	//	m_appExcel.Quit();
	//}
	//if (!m_appExcel.CreateDispatch("Excel.Application",NULL)) 
	//{
	//	MessageBox("创建Excel服务失败!","提示",MB_OK|MB_ICONWARNING|MB_TOPMOST);
	//	return ;
	//} 

	//m_appExcel.SetVisible(false);
	//m_wbExcels.AttachDispatch(m_appExcel.GetWorkbooks(),true); 
	//m_wbExcelSingle.AttachDispatch(m_wbExcels.Add(_variant_t(m_strFilePath)));
	//
	//m_wsExcels.AttachDispatch(m_wbExcelSingle.GetWorksheets(),true);

	//for(int j=1;j<=m_wsExcels.GetCount();j++)
	//{
	//	m_wsExcelSingle.AttachDispatch( m_wsExcels.GetItem( COleVariant( long( j ) ) ), TRUE );
	//	CString sVal =  m_wsExcelSingle.GetName();
	//	
	//	this->m_comboSheet.AddString(sVal);
	//}
	//
	////释放对象 
	//COleVariant covOptional((long)DISP_E_PARAMNOTFOUND, VT_ERROR); 
	//m_wbExcelSingle.Close( covOptional, COleVariant( m_strFilePath ), covOptional );
	//m_wbExcels.Close();
	//m_wsExcelSingle.ReleaseDispatch(); 
	//m_wsExcels.ReleaseDispatch();
	//m_wbExcelSingle.ReleaseDispatch();   
	//m_wbExcels.ReleaseDispatch(); 
}
