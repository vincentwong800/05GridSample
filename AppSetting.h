#pragma once

namespace ApplicationSettings
{
	class MapleFunction:public AppSettings {
	public:
		MapleFunction() {
			setClassName(GETNAME(IDS_MapleFunction));

			Register(GETNAME(IDS_zuobiao16), &strZuobiao16);
			Register(GETNAME(IDS_zuobiao10), &strZuobiao10);
		}
		xString strZuobiao16;
		xString strZuobiao10;
	};

	class YanzhengIP:public AppSettings {
	public:
		YanzhengIP() {
			setClassName(GETNAME(IDS_yanzhengip));

			Register(GETNAME(IDS_yanzhengipfile), &yzipfile);
			Register(GETNAME(IDS_yanzhengipthreadcnt), &yzipthreadcnt, string("10"));
			Register(GETNAME(IDS_yanzhengipfailretry), &yzipretry, string("0"));
			Register(GETNAME(IDS_yanzhengipstart), &yzipstart, string("False"));
		}
		xString yzipfile;
		xInt yzipthreadcnt;
		xInt yzipretry;
		xBool yzipstart;
	};

	class CountDown:public AppSettings {
	public:
		CountDown() {
			setClassName(GETNAME(IDS_DAOJISHI));

			Register(GETNAME(IDS_DAOJISHICOUNT), &daojishi, string("15"));
		}
		xInt daojishi;
	};

	class AesEncrypt : public AppSettings {
	public:
		AesEncrypt() {
			setClassName(GETNAME(IDS_EncryptText));

			Register(GETNAME(IDS_AES_FILE), &aesFile);
			Register(GETNAME(IDS_AES_KEY), &aesKey);
		}
		xString aesFile;
		xString aesKey;
	};

	//////////////////////////////////////////////////////////////////////////
	// �O��
	class AppSetting : public AppSettings
	{
	public:
		AppSetting() {
			setClassName(GETNAME(IDS_ApplicationSettings));

			Register(GETNAME(IDS_MapleFunction), &maple);
			Register(GETNAME(IDS_yanzhengip), &yzip);
			Register(GETNAME(IDS_DAOJISHI), &countdown);
			Register(GETNAME(IDS_EncryptText), &aesenc);
		}
		MapleFunction maple;
		YanzhengIP yzip;
		CountDown countdown;
		AesEncrypt aesenc;
	};
}
