// PropertyGridDlg.h : header file
//
// This file is a part of the XTREME TOOLKIT PRO MFC class library.
// (c)1998-2011 Codejock Software, All Rights Reserved.
//
// THIS SOURCE FILE IS THE PROPERTY OF CODEJOCK SOFTWARE AND IS NOT TO BE
// RE-DISTRIBUTED BY ANY MEANS WHATSOEVER WITHOUT THE EXPRESSED WRITTEN
// CONSENT OF CODEJOCK SOFTWARE.
//
// THIS SOURCE CODE CAN ONLY BE USED UNDER THE TERMS AND CONDITIONS OUTLINED
// IN THE XTREME TOOLKIT PRO LICENSE AGREEMENT. CODEJOCK SOFTWARE GRANTS TO
// YOU (ONE SOFTWARE DEVELOPER) THE LIMITED RIGHT TO USE THIS SOFTWARE ON A
// SINGLE COMPUTER.
//
// CONTACT INFORMATION:
// support@codejock.com
// http://www.codejock.com
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_PropertyGridDLG_H__03B48B0F_47BD_4632_B28B_BCE982A01581__INCLUDED_)
#define AFX_PropertyGridDLG_H__03B48B0F_47BD_4632_B28B_BCE982A01581__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CPropertyGridDlg dialog

#include "AppSetting.h"
#include "afxcmn.h"

typedef struct _tagYanzhenip {
	TString strip;
	TString strport;
} Yanzhenip;


#define CPropertyGridDlgBase CXTPResizeDialog

class CPropertyGridDlg : public CPropertyGridDlgBase
{
// Construction
public:
	CPropertyGridDlg(CWnd* pParent = NULL); // standard constructor

	TString m_setfile;
	AppSetting m_set;

	CList<Yanzhenip, Yanzhenip&> m_listYanzhengip;

// Dialog Data
	//{{AFX_DATA(CPropertyGridDlg)
	enum { IDD = IDD_PROPERTYGRID_DIALOG };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPropertyGridDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	HICON    m_hIcon;
	CPropertyGridEx m_wndPropertyGrid;
	BOOL m_bCollapse;
	BOOL m_bInitUpdate;
	string m_strshuzi[11];
	
protected:
	// Generated message map functions
	//{{AFX_MSG(CPropertyGridDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnDestroy();
	//}}AFX_MSG
	afx_msg LRESULT OnGridNotify(WPARAM, LPARAM);

	COleDateTime m_dtStart;
	BOOL m_bThreadEndLoop;
	CCriticalSection m_csWriteIpFile;
	LONG m_nHandlingCnt;
	LONG m_nHandledCnt;
	LONG m_nHttpOkCnt;
	static DWORD CALLBACK ThreadPool_Yanzhengip(LPVOID p);

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnTimer(UINT_PTR nIDEvent);
public:
//	afx_msg void OnBnClickedButton1();
public:
//	afx_msg void OnBnClickedButton2();
public:
	//CListCtrl m_list1;
public:
//	afx_msg void OnBnClickedButton3();
public:
//	afx_msg void OnBnClickedButton4();
	VOID Grid_OnZuobiao16to10(CXTPPropertyGridItem* pItem);
	VOID Grid_OnZuobiao10to16(CXTPPropertyGridItem* pItem);
	VOID Grid_OnModules(CXTPPropertyGridItem* pItem);
	VOID Grid_OnThreadRead(CXTPPropertyGridItem* pItem);
	VOID Grid_OnNtStatusToDosError(CXTPPropertyGridItem* pItem);
	VOID Grid_GetColor(CXTPPropertyGridItem* pItem);
	VOID Grid_CmpColor(CXTPPropertyGridItem* pItem);
	VOID Grid_OnYanzhengipFile(CXTPPropertyGridItem* pItem);
	VOID Grid_OnYanzhengipStart(CXTPPropertyGridItem* pItem);
	VOID Grid_OnDaojishi(CXTPPropertyGridItem* pItem);
	VOID Grid_OnDaojishiStart(CXTPPropertyGridItem* pItem);
	VOID Grid_OnDaojishiSuspend(CXTPPropertyGridItem* pItem);
	VOID Grid_OnDaojishiResume(CXTPPropertyGridItem* pItem);
	VOID Grid_OnCounterAdd(CXTPPropertyGridItem* pItem);
	VOID Grid_OnCounterSub(CXTPPropertyGridItem* pItem);
	VOID Grid_OnAesFile(CXTPPropertyGridItem* pItem);
	VOID Grid_OnBtnAesEncrypt(CXTPPropertyGridItem* pItem);
public:
	VOID UpdateSheetName(void);
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PropertyGridDLG_H__03B48B0F_47BD_4632_B28B_BCE982A01581__INCLUDED_)
